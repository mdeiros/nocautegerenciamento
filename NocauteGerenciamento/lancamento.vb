﻿Public Class lancamento

    Dim dtDados As DataTable = New DataTable()
    Dim sql = "SELECT * FROM lancamento"
    Dim editMode As Boolean = False
    Dim baseForm As lancamento_consulta

    Public Sub setData(baseForm As Form)
        Me.baseForm = CType(baseForm, lancamento_consulta)
    End Sub

    Public Sub setData(baseForm As Form, dataTable As DataTable)
        Me.baseForm = CType(baseForm, lancamento_consulta)
        editMode = True
        dtDados = dataTable.Copy
        fillFormWithData(dtDados.Rows(0))
    End Sub

    Private Sub fillFormWithData(row As DataRow)
        setInitialValues()
        cmbOperacao.SelectedValue = row("operacao")
        cmbTipoOperacao.SelectedValue = row("idTipoOperacao")
        dpMesAnoBase.Value = Convert.ToDateTime(row("mesAnoBase"))
        txtDescricao.Text = row("descricao")
        txtValor.Text = Convert.ToDouble(row("valor"))
    End Sub

    Private Sub prepareDataStructure()
        If Not editMode Then
            AddIntegerColumnIntoTable(dtDados, "id")
            AddStringColumnIntoTable(dtDados, "operacao")
            AddIntegerColumnIntoTable(dtDados, "idTipoOperacao")
            AddIntegerColumnIntoTable(dtDados, "idTipoBemImobilizado")
            AddDateTimeColumnIntoTable(dtDados, "mesAnoBase")
            AddStringColumnIntoTable(dtDados, "descricao")
            AddDoubleColumnIntoTable(dtDados, "valor")
            setAutoIncrementOnFirstColumn(dtDados)
        End If
    End Sub

    Private Sub prepareToSave()
        Dim valor As Double

        Dim row As DataRow
        If Not editMode Then
            row = dtDados.NewRow
        Else
            row = dtDados.Rows(0)
        End If

        row("operacao") = cmbOperacao.SelectedValue
        row("idTipoOperacao") = cmbTipoOperacao.SelectedValue
        row("mesAnoBase") = dpMesAnoBase.Value
        row("descricao") = txtDescricao.Text
        SetIntoRowIfDouble(row, "valor", "Valor", txtValor.Text, valor)

        If Not editMode Then
            dtDados.Rows.Add(row)
        End If
    End Sub

    Private Sub save()
        mainModule.save(sql, dtDados)
    End Sub

    Private Sub setInitialValues()
        loadOperationsCombo()
        LoadOperationTypeCombo()
        formatDateBasisField()
    End Sub

    Private Sub showOpener()
        baseForm.ReSearch()
    End Sub

    Private Sub loadOperationsCombo()
        lancamentoModule.loadOperationsCombo(cmbOperacao)
    End Sub

    Private Sub LoadOperationTypeCombo()
        lancamentoModule.LoadOperationTypeCombo(cmbTipoOperacao)
    End Sub

    Private Sub formatDateBasisField()
        lancamentoModule.formatDateBasisField(dpMesAnoBase)
    End Sub

    Private Sub lancamento_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.ControlBox = False

        If Not editMode Then
            setInitialValues()
        End If
        prepareDataStructure()
    End Sub

    Private Sub btnGravar_Click(sender As Object, e As EventArgs) Handles btnGravar.Click
        'If cmbOperacao.SelectedIndex = 0 Or cmbTipoOperacao.SelectedItem = 0 Or txtDescricao.Text = 0 Then
        'MsgBox("Preencha todos os campos")
        'Else
        Try
                prepareToSave()
                save()
                Me.Close()
                showOpener()
            Catch ex As Exception
                treatMessage(lblMessage, ex.Message)
            End Try
        'End If
    End Sub

    Private Sub btnFecharSemSalvar_Click(sender As Object, e As EventArgs) Handles btnFecharSemSalvar.Click
        Me.Close()
        showOpener()
    End Sub
End Class