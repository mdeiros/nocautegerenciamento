﻿Imports System.Data.OleDb

Module lancamentoModule

    Public Const OPERACAO_CREDITO As String = "CREDITO"
    Public Const OPERACAO_DEBITO As String = "DEBITO"

    Public Sub loadOperationsCombo(cmb As ComboBox)
        Dim operations As DataTable = New DataTable()
        operations.Columns.Add("COD", Type.GetType("System.String"))
        operations.Columns.Add("VAL", Type.GetType("System.String"))
        operations.Rows.Add("", "Todos")
        operations.Rows.Add(OPERACAO_DEBITO, "Débito")
        operations.Rows.Add(OPERACAO_CREDITO, "Crédito")

        cmb.DataSource = operations
        cmb.ValueMember = "COD"
        cmb.DisplayMember = "VAL"
    End Sub

    Public Sub formatDateBasisField(dp As DateTimePicker)
        dp.CustomFormat = "MMM/yyyy"
        dp.Format = DateTimePickerFormat.Custom
    End Sub

    Private Sub setAlwaysDayOne(monthYearDate As DateTimePicker)
        If monthYearDate.Value.Day <> 1 Then
            monthYearDate.Value = DateSerial(monthYearDate.Value.Year, monthYearDate.Value.Month, 1)
        End If
    End Sub

    Public Function LoadOperationType() As DataTable
        Dim sql As String = "SELECT id, nome, idPai FROM tipooperacao ORDER BY id ASC"
        Dim data As DataTable = New DataTable

        Using conn = NGConnection()
            Using command = NGCommand(sql, conn)
                Using adapter = NGAdapter(command)
                    adapter.Fill(data)
                End Using
            End Using
        End Using

        'interagir nos primeiros niveis da tree (numeraçao e nome)
        Dim dataView As DataView = New DataView(data)
        dataView.RowFilter = "idPai is null"
        Dim rootIndex As Integer = 1
        For Each drv As DataRowView In dataView
            drv.Item("nome") = Convert.ToString(rootIndex) & ". " & drv.Item("nome")
            setSubItemsNumbering(dataView, Convert.ToInt32(drv.Item("id")), Convert.ToString(rootIndex))
            rootIndex = rootIndex + 1
        Next

        dataView.RowFilter = Nothing
        dataView.Sort = "nome"
        Return dataView.ToTable
    End Function

    'interagir nos proximos niveis da tree (numeraçao e nome) com recursão
    Private Sub setSubItemsNumbering(dataView As DataView, parentId As Integer, index As String)
        dataView.RowFilter = "idPai = " & parentId
        If dataView.Count = 0 Then
            Return
        End If

        Dim subIndex As Integer = 1
        For Each drv As DataRowView In dataView
            drv.Item("nome") = index & "." & subIndex & ". " & drv.Item("nome")
            setSubItemsNumbering(dataView, drv.Item("id"), index & "." & subIndex)
            subIndex = subIndex + 1
        Next
    End Sub

    'converte a estrutura em combo
    Public Sub LoadOperationTypeCombo(cmb As ComboBox)
        Dim data As DataTable = LoadOperationType()
        Dim firstRow As DataRow = data.NewRow
        cmb.DataSource = data
        cmb.ValueMember = "id"
        cmb.DisplayMember = "nome"

        firstRow.Item(0) = 0
        firstRow.Item(1) = "Todos"
        data.Rows.InsertAt(firstRow, 0)
        cmb.SelectedIndex = 0
    End Sub

    'treeview utils (coloca os pontinhos)
    Public Function addDotsUntilRightSize(initialSize)
        Return New String(".", 45 - initialSize)
    End Function

    Public Function addLeftDotsUntilRightSize(str As String)
        Return New String(".", 16 - str.Count) & str
    End Function

    Public Function TreeViewItemTextWithoutNumbering(treeViewItem As String)
        Return Mid(treeViewItem, InStr(treeViewItem, " ") + 1)
    End Function

    'layout da tree view
    Public Function createTreeviewItemText(table As DataTable, rowIndex As Integer, itemName As String, value As Double)
        Return table.Rows(rowIndex).Item(itemName) &
            lancamentoModule.addDotsUntilRightSize(CType(table.Rows(rowIndex).Item(itemName), String).Count) &
            lancamentoModule.addLeftDotsUntilRightSize(Convert.ToDecimal(value).ToString("c"))
    End Function

    Public Function calculateTreeViewItemValue(operacao As String, tipoOperacaoNome As String,
        value As Double, tipoOperacaoItem As String, currentValue As Double)
        If tipoOperacaoNome = tipoOperacaoItem Then
            If operacao = "CREDITO" Then
                Return currentValue + value
            ElseIf operacao = "DEBITO" Then
                Return currentValue - value
            Else
                Return currentValue
            End If
        Else
            Return currentValue
        End If
    End Function


End Module
