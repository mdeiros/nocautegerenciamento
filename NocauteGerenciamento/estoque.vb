﻿Public Class estoque
    Dim dtDados As DataTable = New DataTable()
    Dim sql = "SELECT * FROM estoque"
    Dim editMode As Boolean = False
    Dim baseForm As estoque_consulta

    Public Sub setData(baseForm As Form)
        Me.baseForm = CType(baseForm, estoque_consulta)
    End Sub

    Public Sub setData(baseForm As Form, dataTable As DataTable)
        Me.baseForm = CType(baseForm, estoque_consulta)
        editMode = True
        dtDados = dataTable.Copy
        fillFormWithData(dtDados.Rows(0))
    End Sub

    Private Sub fillFormWithData(row As DataRow)
        setInitialValues()
        txtProduto.Text = row("nomeItem")
        txtQuantidade.Text = Convert.ToInt32(row("qtde"))
        txtValor.Text = Convert.ToDouble(row("valor"))
        dtpData.Value = Convert.ToDateTime(row("data"))
    End Sub

    Private Sub prepareDataStructure()
        If Not editMode Then
            AddIntegerColumnIntoTable(dtDados, "id")
            AddStringColumnIntoTable(dtDados, "nomeItem")
            AddIntegerColumnIntoTable(dtDados, "qtde")
            AddDoubleColumnIntoTable(dtDados, "valor")
            AddDateTimeColumnIntoTable(dtDados, "data")
            setAutoIncrementOnFirstColumn(dtDados)
        End If
    End Sub

    Private Sub prepareToSave()
        Dim qtde As Integer
        Dim valor As Double

        Dim row As DataRow
        If Not editMode Then
            row = dtDados.NewRow
        Else
            row = dtDados.Rows(0)
        End If

        row("nomeItem") = txtProduto.Text
        SetIntoRowIfInteger(row, "qtde", "Qtde", txtQuantidade.Text, qtde)
        SetIntoRowIfDouble(row, "valor", "Valor", txtValor.Text, valor)
        row("data") = dtpData.Value

        If Not editMode Then
            dtDados.Rows.Add(row)
        End If
    End Sub

    Private Sub save()
        mainModule.save(sql, dtDados)
    End Sub

    Private Sub setInitialValues()
        dtpData.CustomFormat = "dd/MM/yyyy"
        dtpData.Format = DateTimePickerFormat.Custom
    End Sub

    Private Sub showOpener()
        baseForm.ReSearch()
    End Sub

    Private Sub estoque_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.ControlBox = False

        If Not editMode Then
            setInitialValues()
        End If
        prepareDataStructure()
    End Sub

    Private Sub btnGravar_Click(sender As Object, e As EventArgs) Handles btnGravar.Click
        If txtProduto.Text = "" Or txtQuantidade.Text = "" Or txtValor.Text = "" Then
            MsgBox("Preencha todos os campos")
        Else
            Try
                prepareToSave()
                save()
                Me.Close()
                showOpener()
            Catch ex As Exception
                treatMessage(lblMessage, ex.Message)
            End Try
        End If
    End Sub

    Private Sub btnFecharSemSalvar_Click(sender As Object, e As EventArgs) Handles btnFecharSemSalvar.Click
        Me.Close()
        showOpener()
    End Sub

End Class