﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class lancamento_consulta
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: o procedimento a seguir é exigido pelo Windows Form Designer
    'Pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.txtDescricao = New System.Windows.Forms.TextBox()
        Me.cmbTipoOperacao = New System.Windows.Forms.ComboBox()
        Me.cmbOperacao = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnRelatorio = New System.Windows.Forms.Button()
        Me.btnExcluirSelecionados = New System.Windows.Forms.Button()
        Me.btnEditarSelecionado = New System.Windows.Forms.Button()
        Me.btnNovo = New System.Windows.Forms.Button()
        Me.dgDados = New System.Windows.Forms.DataGridView()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.lblMensagem = New System.Windows.Forms.Label()
        Me.bindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.tvTipoOperacao = New System.Windows.Forms.TreeView()
        Me.dtpDataFim = New System.Windows.Forms.DateTimePicker()
        Me.dtpDataInicio = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgDados, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.bindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dtpDataFim)
        Me.GroupBox1.Controls.Add(Me.dtpDataInicio)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.btnBuscar)
        Me.GroupBox1.Controls.Add(Me.txtDescricao)
        Me.GroupBox1.Controls.Add(Me.cmbTipoOperacao)
        Me.GroupBox1.Controls.Add(Me.cmbOperacao)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 13)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(443, 168)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Consulta"
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(351, 133)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(75, 23)
        Me.btnBuscar.TabIndex = 8
        Me.btnBuscar.Text = "Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'txtDescricao
        '
        Me.txtDescricao.Location = New System.Drawing.Point(214, 102)
        Me.txtDescricao.Name = "txtDescricao"
        Me.txtDescricao.Size = New System.Drawing.Size(212, 20)
        Me.txtDescricao.TabIndex = 7
        '
        'cmbTipoOperacao
        '
        Me.cmbTipoOperacao.FormattingEnabled = True
        Me.cmbTipoOperacao.Location = New System.Drawing.Point(214, 53)
        Me.cmbTipoOperacao.Name = "cmbTipoOperacao"
        Me.cmbTipoOperacao.Size = New System.Drawing.Size(212, 21)
        Me.cmbTipoOperacao.TabIndex = 6
        '
        'cmbOperacao
        '
        Me.cmbOperacao.FormattingEnabled = True
        Me.cmbOperacao.Location = New System.Drawing.Point(19, 53)
        Me.cmbOperacao.Name = "cmbOperacao"
        Me.cmbOperacao.Size = New System.Drawing.Size(186, 21)
        Me.cmbOperacao.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(211, 86)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(55, 13)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Descricao"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(211, 36)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(93, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Tipo da Operacao"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(16, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Operacao"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnRelatorio)
        Me.GroupBox2.Controls.Add(Me.btnExcluirSelecionados)
        Me.GroupBox2.Controls.Add(Me.btnEditarSelecionado)
        Me.GroupBox2.Controls.Add(Me.btnNovo)
        Me.GroupBox2.Controls.Add(Me.dgDados)
        Me.GroupBox2.Location = New System.Drawing.Point(13, 189)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(443, 224)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Grid de dados"
        '
        'btnRelatorio
        '
        Me.btnRelatorio.Location = New System.Drawing.Point(342, 181)
        Me.btnRelatorio.Name = "btnRelatorio"
        Me.btnRelatorio.Size = New System.Drawing.Size(95, 23)
        Me.btnRelatorio.TabIndex = 4
        Me.btnRelatorio.Text = "Relatório"
        Me.btnRelatorio.UseVisualStyleBackColor = True
        '
        'btnExcluirSelecionados
        '
        Me.btnExcluirSelecionados.Location = New System.Drawing.Point(214, 181)
        Me.btnExcluirSelecionados.Name = "btnExcluirSelecionados"
        Me.btnExcluirSelecionados.Size = New System.Drawing.Size(121, 23)
        Me.btnExcluirSelecionados.TabIndex = 3
        Me.btnExcluirSelecionados.Text = "Excluir Selecionados"
        Me.btnExcluirSelecionados.UseVisualStyleBackColor = True
        '
        'btnEditarSelecionado
        '
        Me.btnEditarSelecionado.Location = New System.Drawing.Point(91, 181)
        Me.btnEditarSelecionado.Name = "btnEditarSelecionado"
        Me.btnEditarSelecionado.Size = New System.Drawing.Size(117, 23)
        Me.btnEditarSelecionado.TabIndex = 2
        Me.btnEditarSelecionado.Text = "Editar Selecionado"
        Me.btnEditarSelecionado.UseVisualStyleBackColor = True
        '
        'btnNovo
        '
        Me.btnNovo.Location = New System.Drawing.Point(10, 181)
        Me.btnNovo.Name = "btnNovo"
        Me.btnNovo.Size = New System.Drawing.Size(75, 23)
        Me.btnNovo.TabIndex = 1
        Me.btnNovo.Text = "Novo"
        Me.btnNovo.UseVisualStyleBackColor = True
        '
        'dgDados
        '
        Me.dgDados.AllowUserToAddRows = False
        Me.dgDados.AllowUserToDeleteRows = False
        Me.dgDados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgDados.Location = New System.Drawing.Point(7, 20)
        Me.dgDados.Name = "dgDados"
        Me.dgDados.ReadOnly = True
        Me.dgDados.Size = New System.Drawing.Size(430, 154)
        Me.dgDados.TabIndex = 0
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lblMensagem)
        Me.GroupBox3.Location = New System.Drawing.Point(13, 419)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(443, 49)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Mensagem"
        '
        'lblMensagem
        '
        Me.lblMensagem.Location = New System.Drawing.Point(7, 20)
        Me.lblMensagem.Name = "lblMensagem"
        Me.lblMensagem.Size = New System.Drawing.Size(430, 23)
        Me.lblMensagem.TabIndex = 0
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.tvTipoOperacao)
        Me.GroupBox4.Location = New System.Drawing.Point(463, 13)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(453, 455)
        Me.GroupBox4.TabIndex = 3
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Tipos de Operação"
        '
        'tvTipoOperacao
        '
        Me.tvTipoOperacao.Location = New System.Drawing.Point(7, 53)
        Me.tvTipoOperacao.Name = "tvTipoOperacao"
        Me.tvTipoOperacao.Size = New System.Drawing.Size(440, 347)
        Me.tvTipoOperacao.TabIndex = 0
        '
        'dtpDataFim
        '
        Me.dtpDataFim.Location = New System.Drawing.Point(115, 102)
        Me.dtpDataFim.Name = "dtpDataFim"
        Me.dtpDataFim.Size = New System.Drawing.Size(90, 20)
        Me.dtpDataFim.TabIndex = 18
        '
        'dtpDataInicio
        '
        Me.dtpDataInicio.Location = New System.Drawing.Point(19, 102)
        Me.dtpDataInicio.Name = "dtpDataInicio"
        Me.dtpDataInicio.Size = New System.Drawing.Size(90, 20)
        Me.dtpDataInicio.TabIndex = 17
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(16, 86)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(45, 13)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Período"
        '
        'lancamento_consulta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(928, 481)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "lancamento_consulta"
        Me.Text = "lancamento_consulta"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dgDados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.bindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtDescricao As TextBox
    Friend WithEvents cmbTipoOperacao As ComboBox
    Friend WithEvents cmbOperacao As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents btnBuscar As Button
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents dgDados As DataGridView
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents lblMensagem As Label
    Friend WithEvents btnNovo As Button
    Friend WithEvents btnExcluirSelecionados As Button
    Friend WithEvents btnEditarSelecionado As Button
    Friend WithEvents bindingSource As BindingSource
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents tvTipoOperacao As TreeView
    Friend WithEvents btnRelatorio As Button
    Friend WithEvents dtpDataFim As DateTimePicker
    Friend WithEvents dtpDataInicio As DateTimePicker
    Friend WithEvents Label3 As Label
End Class
