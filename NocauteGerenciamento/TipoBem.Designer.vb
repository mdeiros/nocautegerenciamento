﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class TipoBem
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: o procedimento a seguir é exigido pelo Windows Form Designer
    'Pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.dgDados = New System.Windows.Forms.DataGridView()
        Me.lblNome = New System.Windows.Forms.Label()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtNome = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnRelatorio = New System.Windows.Forms.Button()
        Me.lblHelpGrid = New System.Windows.Forms.Label()
        Me.lblHelpSearch = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.lblMensagem = New System.Windows.Forms.Label()
        Me.bindingSource = New System.Windows.Forms.BindingSource(Me.components)
        CType(Me.dgDados, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.bindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(281, 87)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(105, 23)
        Me.btnBuscar.TabIndex = 0
        Me.btnBuscar.Text = "Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'dgDados
        '
        Me.dgDados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgDados.Location = New System.Drawing.Point(9, 39)
        Me.dgDados.Name = "dgDados"
        Me.dgDados.Size = New System.Drawing.Size(406, 194)
        Me.dgDados.TabIndex = 1
        '
        'lblNome
        '
        Me.lblNome.AutoSize = True
        Me.lblNome.Location = New System.Drawing.Point(18, 25)
        Me.lblNome.Name = "lblNome"
        Me.lblNome.Size = New System.Drawing.Size(35, 13)
        Me.lblNome.TabIndex = 2
        Me.lblNome.Text = "Nome"
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(6, 239)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(191, 23)
        Me.btnDelete.TabIndex = 5
        Me.btnDelete.Text = "Excluir linhas selecionadas"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnBuscar)
        Me.GroupBox1.Controls.Add(Me.lblNome)
        Me.GroupBox1.Controls.Add(Me.txtNome)
        Me.GroupBox1.Location = New System.Drawing.Point(122, 9)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(434, 127)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Consulta"
        '
        'txtNome
        '
        Me.txtNome.Location = New System.Drawing.Point(21, 61)
        Me.txtNome.Name = "txtNome"
        Me.txtNome.Size = New System.Drawing.Size(365, 20)
        Me.txtNome.TabIndex = 3
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnRelatorio)
        Me.GroupBox2.Controls.Add(Me.dgDados)
        Me.GroupBox2.Controls.Add(Me.btnDelete)
        Me.GroupBox2.Location = New System.Drawing.Point(122, 142)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(434, 273)
        Me.GroupBox2.TabIndex = 7
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Grid de dados"
        '
        'btnRelatorio
        '
        Me.btnRelatorio.Location = New System.Drawing.Point(214, 239)
        Me.btnRelatorio.Name = "btnRelatorio"
        Me.btnRelatorio.Size = New System.Drawing.Size(201, 23)
        Me.btnRelatorio.TabIndex = 6
        Me.btnRelatorio.Text = "Gerar Relatório"
        Me.btnRelatorio.UseVisualStyleBackColor = True
        '
        'lblHelpGrid
        '
        Me.lblHelpGrid.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHelpGrid.Location = New System.Drawing.Point(575, 151)
        Me.lblHelpGrid.Name = "lblHelpGrid"
        Me.lblHelpGrid.Size = New System.Drawing.Size(199, 229)
        Me.lblHelpGrid.TabIndex = 8
        Me.lblHelpGrid.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblHelpSearch
        '
        Me.lblHelpSearch.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHelpSearch.Location = New System.Drawing.Point(578, 9)
        Me.lblHelpSearch.Name = "lblHelpSearch"
        Me.lblHelpSearch.Size = New System.Drawing.Size(196, 105)
        Me.lblHelpSearch.TabIndex = 9
        Me.lblHelpSearch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lblMensagem)
        Me.GroupBox3.Location = New System.Drawing.Point(122, 421)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(611, 40)
        Me.GroupBox3.TabIndex = 10
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Mensagem"
        '
        'lblMensagem
        '
        Me.lblMensagem.Location = New System.Drawing.Point(15, 16)
        Me.lblMensagem.Name = "lblMensagem"
        Me.lblMensagem.Size = New System.Drawing.Size(596, 20)
        Me.lblMensagem.TabIndex = 0
        '
        'TipoBem
        '
        Me.AcceptButton = Me.btnBuscar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(876, 467)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.lblHelpSearch)
        Me.Controls.Add(Me.lblHelpGrid)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "TipoBem"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TipoBem"
        CType(Me.dgDados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.bindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnBuscar As Button
    Friend WithEvents dgDados As DataGridView
    Friend WithEvents lblNome As Label
    Friend WithEvents btnDelete As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents lblHelpGrid As Label
    Friend WithEvents lblHelpSearch As Label
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents lblMensagem As Label
    Friend WithEvents bindingSource As BindingSource
    Friend WithEvents btnRelatorio As Button
    Friend WithEvents txtNome As TextBox
End Class
