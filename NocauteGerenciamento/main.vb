﻿Public Class main

    Dim nomeAplicacao = "NocauteDB"

    Private loginForm As Login

    Public Sub setLoginForm(loginForm As Login)
        Me.loginForm = loginForm
    End Sub

    Private Sub prepareMDIContainer()
        Dim ctl As Control
        Dim ctlMDI As MdiClient
        For Each ctl In Me.Controls
            Try
                ctlMDI = CType(ctl, MdiClient)
                ctlMDI.BackColor = Me.BackColor
            Catch exc As InvalidCastException
                'just ignore without errors
            End Try
        Next
    End Sub

    Public Sub prepareFormForMDI(form As Form)
        form.MdiParent = Me
        form.WindowState = FormWindowState.Maximized
        form.MaximumSize = Screen.PrimaryScreen.WorkingArea.Size
        form.StartPosition = FormStartPosition.CenterScreen
    End Sub

    Private Sub DefineWindowTitle(form As Form, formName As String)
        form.Text = nomeAplicacao & " - " & formName
    End Sub

    Private Sub goForm(form As Form, formName As String)
        DefineWindowTitle(form, formName)
        prepareFormForMDI(form)
        form.Show()
    End Sub

    Private Sub showFormAdministracaoUsuarios()
        goForm(New administracaousuarios_consulta, mainModule.FORMNAME_ADMUSUARIOS)
    End Sub

    Private Sub showFormTipoDeBem()
        goForm(New TipoBem, mainModule.FORMNAME_TIPOBEM)
    End Sub

    Private Sub showFormBem()
        goForm(New bens_consulta, mainModule.FORMNAME_BEM)
    End Sub

    Private Sub showFormSuporte()
        goForm(New suporte, mainModule.FORMNAME_SUPORTE)
    End Sub

    Private Sub showFormLancamento()
        goForm(New lancamento_consulta, mainModule.FORMNAME_LANCAMENTO)
    End Sub

    Private Sub showFormEstoque()
        goForm(New estoque_consulta, mainModule.FORMNAME_ESTOQUE)
    End Sub

    Private Sub showFormMovimento()
        goForm(New movimento_consulta, mainModule.FORMNAME_MOVIMENTO)
    End Sub

    Private Sub showFormDados()
        goForm(New dados, mainModule.FORMNAME_DADOS)
    End Sub

    'Para ser chamado pelos relatórios e retornar para tela original (para qual ele foi aberto)
    Public Sub showForm(formName As String)
        If formName = mainModule.FORMNAME_ADMUSUARIOS Then
            showFormAdministracaoUsuarios()
        ElseIf formName = mainModule.FORMNAME_TIPOBEM Then
            showFormTipoDeBem()
        ElseIf formName = mainModule.FORMNAME_BEM Then
            showFormBem()
        ElseIf formName = mainModule.FORMNAME_LANCAMENTO Then
            showFormLancamento()
        ElseIf formName = mainModule.FORMNAME_SUPORTE Then
            showFormSuporte()
        ElseIf formName = mainModule.FORMNAME_ESTOQUE Then
            showFormEstoque()
        ElseIf formName = mainModule.FORMNAME_MOVIMENTO Then
            showFormMovimento()
        ElseIf formName = mainModule.FORMNAME_DADOS Then
            showFormDados()
        End If
    End Sub

    Private Sub prepareUserMenu(role As String)
        Select Case role
            Case mainModule.USERROLE_ADMIN
                AdministraçãoToolStripMenuItem.Visible = True
                MovimentosToolStripMenuItem.Visible = True
                GestãoContábilToolStripMenuItem.Visible = True
            Case mainModule.USERROLE_CONTADOR
                AdministraçãoToolStripMenuItem.Visible = False
                MovimentosToolStripMenuItem.Visible = False
                GestãoContábilToolStripMenuItem.Visible = True
            Case mainModule.USERROLE_FUNCIONARIO
                AdministraçãoToolStripMenuItem.Visible = False
                MovimentosToolStripMenuItem.Visible = True
                GestãoContábilToolStripMenuItem.Visible = False
        End Select
    End Sub

    Private Sub main_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        prepareUserMenu(mainModule.currentUserRole)
        prepareMDIContainer()
    End Sub

    Private Sub TipoDeBemToolStripMenuItem_Click(sender As Object, e As EventArgs)
        showFormTipoDeBem()
    End Sub

    Private Sub BensToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BensToolStripMenuItem1.Click
        showFormBem()
    End Sub

    Private Sub LançamentoToolStripMenuItem_Click(sender As Object, e As EventArgs)
        showFormLancamento()
    End Sub

    Private Sub SuporteToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles SuporteToolStripMenuItem1.Click
        showFormSuporte()
    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        mainModule.currentUserRole = Nothing
        Me.Close()
        loginForm.clearFields()
        loginForm.Show()
    End Sub

    Private Sub TipoDeBemToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles TipoDeBemToolStripMenuItem1.Click
        showFormTipoDeBem()
    End Sub

    Private Sub LançamentosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LançamentosToolStripMenuItem.Click
        showFormLancamento()
    End Sub

    Private Sub AdministraçãoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AdministraçãoToolStripMenuItem.Click
        showFormAdministracaoUsuarios()
    End Sub

    Private Sub EstoqueToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EstoqueToolStripMenuItem.Click
        showFormEstoque()
    End Sub

    Private Sub MovimentosToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles MovimentosToolStripMenuItem1.Click
        showFormMovimento()
    End Sub

    Private Sub btnDados_Click(sender As Object, e As EventArgs) Handles btnDados.Click
        showFormDados()
    End Sub
End Class