﻿Module bensModule

    ' calculations
    Public Function calcularValorADepreciar(creationDate As DateTime, prazoMeses As Int32, valor As Double, taxaMensalDepreciacao As Double)
        Dim diff As Long = DateDiff(DateInterval.Month, creationDate, DateTime.Now)

        If diff <= prazoMeses Then
            Return valor * taxaMensalDepreciacao / 100 * diff
        Else
            Return valor
        End If
    End Function

    Public Function calcularValorAtualizado(valor As Double, valorADepreciar As Double)
        Return valor - valorADepreciar
    End Function

End Module
