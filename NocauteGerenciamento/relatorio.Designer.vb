﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class relatorio
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: o procedimento a seguir é exigido pelo Windows Form Designer
    'Pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(relatorio))
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btnFechar = New System.Windows.Forms.Button()
        Me.lblFilePath = New System.Windows.Forms.Label()
        Me.axPDF = New AxAcroPDFLib.AxAcroPDF()
        Me.GroupBox3.SuspendLayout()
        CType(Me.axPDF, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnFechar)
        Me.GroupBox3.Controls.Add(Me.lblFilePath)
        Me.GroupBox3.Controls.Add(Me.axPDF)
        Me.GroupBox3.Location = New System.Drawing.Point(89, 12)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(746, 459)
        Me.GroupBox3.TabIndex = 4
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Visualizador"
        '
        'btnFechar
        '
        Me.btnFechar.Location = New System.Drawing.Point(3, 427)
        Me.btnFechar.Name = "btnFechar"
        Me.btnFechar.Size = New System.Drawing.Size(120, 23)
        Me.btnFechar.TabIndex = 2
        Me.btnFechar.Text = "Fechar relatório"
        Me.btnFechar.UseVisualStyleBackColor = True
        '
        'lblFilePath
        '
        Me.lblFilePath.Location = New System.Drawing.Point(7, 20)
        Me.lblFilePath.Name = "lblFilePath"
        Me.lblFilePath.Size = New System.Drawing.Size(733, 23)
        Me.lblFilePath.TabIndex = 1
        '
        'axPDF
        '
        Me.axPDF.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.axPDF.Enabled = True
        Me.axPDF.Location = New System.Drawing.Point(3, 46)
        Me.axPDF.Name = "axPDF"
        Me.axPDF.OcxState = CType(resources.GetObject("axPDF.OcxState"), System.Windows.Forms.AxHost.State)
        Me.axPDF.Size = New System.Drawing.Size(740, 375)
        Me.axPDF.TabIndex = 0
        '
        'relatorio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(919, 505)
        Me.Controls.Add(Me.GroupBox3)
        Me.Name = "relatorio"
        Me.Text = "relatorio"
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.axPDF, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents axPDF As AxAcroPDFLib.AxAcroPDF
    Friend WithEvents lblFilePath As Label
    Friend WithEvents btnFechar As Button
End Class
