﻿Public Class bens

    Dim dtDados As DataTable = New DataTable
    Dim sql = "SELECT * FROM bem"
    Dim sqlTipoBem = "SELECT id, nome FROM tipobem"
    Dim editMode As Boolean = False
    Dim baseForm As bens_consulta

    Public Sub setData(baseForm As Form)
        Me.baseForm = CType(baseForm, bens_consulta)
    End Sub

    Public Sub setData(baseForm As Form, dataTable As DataTable)
        Me.baseForm = CType(baseForm, bens_consulta)
        editMode = True
        dtDados = dataTable.Copy
        fillFormWithData(dtDados.Rows(0))
    End Sub

    Private Sub fillFormWithData(row As DataRow)
        setInitialValues()
        SetColumnValueIntoTextboxIfNotNull(row, "descricao", txtDescricao)
        SetColumnValueIntoTextboxIfNotNull(row, "numeroNotaFiscal", txtNumeroNotaFiscal)
        SetColumnValueIntoTextboxIfNotNull(row, "valor", txtValor)
        SetColumnValueIntoTextboxIfNotNull(row, "descricaoDepreciacao", txtDescricaoDepreciacao)
        SetColumnValueIntoTextboxIfNotNull(row, "prazoMeses", txtPrazoMeses)
        lblDataCriacao.Text = row("dataCriacao")
        If Not IsDBNull(row("taxaMensalDepreciacao")) Then
            txtTaxaMensalDepreciacao.Text = Convert.ToDouble(row("taxaMensalDepreciacao")) * 100
        End If
    End Sub

    Private Sub prepareDataStructure()
        If Not editMode Then
            AddIntegerColumnIntoTable(dtDados, "codPatrimonio")
            AddStringColumnIntoTable(dtDados, "descricao")
            AddIntegerColumnIntoTable(dtDados, "numeroNotaFiscal")
            AddDoubleColumnIntoTable(dtDados, "valor")
            AddStringColumnIntoTable(dtDados, "descricaoDepreciacao")
            AddIntegerColumnIntoTable(dtDados, "prazoMeses")
            AddDateTimeColumnIntoTable(dtDados, "dataCriacao")
            AddDoubleColumnIntoTable(dtDados, "taxaMensalDepreciacao")
            AddIntegerColumnIntoTable(dtDados, "tipoBem")
            setAutoIncrementOnFirstColumn(dtDados)
        End If
    End Sub

    Private Sub prepareToSave()
        Dim valor As Double
        Dim numeroNotaFiscal As Integer
        Dim prazoMeses As Integer
        Dim taxaMensalDepreciacao As Double

        Dim row As DataRow
        If Not editMode Then
            row = dtDados.NewRow
        Else
            row = dtDados.Rows(0)
        End If

        row("descricao") = txtDescricao.Text
        SetIntoRowIfInteger(row, "numeroNotaFiscal", "Numero nota fiscal", txtNumeroNotaFiscal.Text, numeroNotaFiscal)
        SetIntoRowIfDouble(row, "valor", "Valor", txtValor.Text, valor)
        row("descricaoDepreciacao") = txtDescricaoDepreciacao.Text
        SetIntoRowIfInteger(row, "prazoMeses", "Prazo (meses)", txtPrazoMeses.Text, prazoMeses)
        row("dataCriacao") = DateTime.Now
        SetIntoRowIfDouble(row, "taxaMensalDepreciacao", "Taxa mensal depreciação", txtTaxaMensalDepreciacao.Text, taxaMensalDepreciacao)
        row("taxaMensalDepreciacao") = row("taxaMensalDepreciacao") / 100
        row("tipoBem") = Convert.ToInt32(cmbTipoBem.SelectedValue)

        If Not editMode Then
            dtDados.Rows.Add(row)
        End If
    End Sub

    Private Sub save()
        mainModule.save(sql, dtDados)

        Dim row As DataRow = dtDados.Rows(0)
        Dim descricaoBem As String = row("descricao")

        If Not editMode Then
            Dim tipoBemId As Integer = Convert.ToInt32(row("tipoBem"))
            Dim parentId As Integer = getParentIdFromBem(tipoBemId)
            'salva no bem e também no tipo de operação
            mainModule.InsertOperationRelatedToBem(parentId, descricaoBem)
        Else
            Dim idBem As Integer = Convert.ToInt32(row("codPatrimonio"))
            mainModule.UpdateOperationRelatedToBem(descricaoBem, idBem)
        End If
    End Sub

    Private Sub setInitialValues()
        lblDataCriacao.Text = DateTime.Now.ToShortDateString

        cmbTipoBem.DataSource = mainModule.load(sqlTipoBem)
        cmbTipoBem.ValueMember = "id"
        cmbTipoBem.DisplayMember = "nome"

        If editMode Then
            cmbTipoBem.SelectedValue = Convert.ToInt32(dtDados.Rows(0).Item("tipoBem"))
            lblDataCriacao.Text = Convert.ToDateTime(dtDados.Rows(0).Item("dataCriacao")).ToShortDateString
        End If

        calcularValores()
    End Sub

    Private Sub showOpener()
        baseForm.ReSearch()
    End Sub

    Private Sub calcularValores()
        Dim prazoMeses As Integer
        Dim taxaMensalDepreciacao As Double
        Dim valor As Double

        If Int32.TryParse(txtPrazoMeses.Text, prazoMeses) And
            Double.TryParse(txtTaxaMensalDepreciacao.Text, taxaMensalDepreciacao) And
            Double.TryParse(txtValor.Text, valor) Then

            Dim valorADepreciar As Double = bensModule.calcularValorADepreciar(Convert.ToDateTime(lblDataCriacao.Text),
                prazoMeses, valor, taxaMensalDepreciacao)
            Dim valorAtualizado As Double = bensModule.calcularValorAtualizado(valor, valorADepreciar)

            lblValorDepreciar.Text = formatDoubleToBrazilianPortugueseCulture(valorADepreciar)
            lblValorAtualizadoBem.Text = formatDoubleToBrazilianPortugueseCulture(valorAtualizado)
        End If
    End Sub

    Private Sub bens_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.ControlBox = False

        If Not editMode Then
            setInitialValues()
        Else
            calcularValores()
        End If
        prepareDataStructure()
    End Sub

    Private Sub btnSalvar_Click(sender As Object, e As EventArgs) Handles btnSalvar.Click
        If txtDescricao.Text = "" Or txtNumeroNotaFiscal.Text = "" Or txtValor.Text = "" Then
            MsgBox("Preencha todos os campos")
        Else
            Try
            prepareToSave()
            save()
            Me.Close()
            showOpener()
        Catch ex As Exception
            treatMessage(lblMessage, ex.Message)
        End Try
        End If
    End Sub

    Private Sub btnFecharSemSalvar_Click(sender As Object, e As EventArgs) Handles btnFecharSemSalvar.Click
        Me.Close()
        showOpener()
    End Sub

    Private Sub txtPrazoMeses_Leave(sender As Object, e As EventArgs) Handles txtPrazoMeses.Leave
        calcularValores()
    End Sub

    Private Sub txtTaxaMensalDepreciacao_Leave(sender As Object, e As EventArgs) Handles txtTaxaMensalDepreciacao.Leave
        calcularValores()
    End Sub

    Private Sub txtValor_Leave(sender As Object, e As EventArgs) Handles txtValor.Leave
        calcularValores()
    End Sub

    Private Sub bens_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
            showOpener()
        End If
    End Sub
End Class