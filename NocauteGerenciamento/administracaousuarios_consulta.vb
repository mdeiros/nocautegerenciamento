﻿Imports System.Data.OleDb

Public Class administracaousuarios_consulta

    Private Function DefineSQL(username As String, role As String, bloqueado As String)
        'monta a sql de consulta
        Dim sql = "SELECT id, username, password, role, bloqueado FROM login WHERE 1=1 "
        If Trim(username) <> "" Then
            sql = sql & "AND username like @username "
        End If
        If Trim(role) <> "" Then
            sql = sql & "AND role=@role "
        End If
        If Trim(bloqueado) <> "" Then
            sql = sql & "AND bloqueado=@bloqueado "
        End If
        sql = sql & "ORDER BY username ASC"
        Return sql
    End Function

    Private Function DefineCommandWithSQLParameters(sql As String, conn As OleDbConnection, username As String, role As String, bloqueado As String)
        'relê o sql substitui os parâmetros acima pelo que foi imputado pelo usuário
        Dim command As OleDbCommand = NGCommand(sql, conn)
        If Trim(username) <> "" Then
            command.Parameters.AddWithValue("@username", "%" & username & "%")
        End If
        If Trim(role) <> "" Then
            command.Parameters.AddWithValue("@role", role)
        End If
        If Trim(bloqueado) <> "" Then
            command.Parameters.AddWithValue("@bloqueado", bloqueado)
        End If
        Return command
    End Function

    Public Sub loadDataGrid(username As String, role As String, bloqueado As String)
        Dim sql = DefineSQL(username, role, bloqueado)
        Using conn = NGConnection()
            Using command = DefineCommandWithSQLParameters(sql, conn, username, role, bloqueado)
                Using adapter = NGAdapter(command)
                    Dim table As DataTable = New DataTable
                    adapter.Fill(table)
                    bindingSource.DataSource = table
                End Using
            End Using
        End Using
    End Sub

    Private Sub hideUndesiredColumns(grid As DataGridView)
        mainModule.hideUndesiredColumns(grid, {"id", "password"})
    End Sub

    Private Sub adjustColumnFormatting(grid As DataGridView)
        dgDados.Columns(1).HeaderCell.Value = "Usuário"
        dgDados.Columns(1).Width = 280
        dgDados.Columns(3).HeaderCell.Value = "Perfil de acesso"
        dgDados.Columns(3).Width = 280
        dgDados.Columns(4).HeaderCell.Value = "Bloqueado?"
        dgDados.Columns(4).Width = 100
    End Sub

    Public Sub clearSearchFilter()
        txtUsuario.Text = ""
        cmbRoles.SelectedIndex = 0
        cmbBloqueado.SelectedIndex = 0
    End Sub

    Public Sub ReSearch()
        loadDataGrid(txtUsuario.Text, cmbRoles.SelectedValue, cmbBloqueado.SelectedValue)
        clearMessage(lblMensagem)
    End Sub

    'cria novo formulário
    Private Sub OpenWindowForCreation()
        Dim administracaoUsuarioNew As New administracaousuarios()
        administracaoUsuarioNew.setData(Me)
        OpenWindowForUpdate(administracaoUsuarioNew)
    End Sub

    'cria formulário com dados do grid para edição
    Private Sub OpenWindowForEdit()
        Dim administracaoUsuariosEdit As New administracaousuarios()
        'seleciona a primeira linha do grid pra seleção e passa os dados pra novo form (que foi criado)
        administracaoUsuariosEdit.setData(Me, mainModule.OpenWindowForEdit(CType(bindingSource.DataSource, DataTable), dgDados))
        OpenWindowForUpdate(administracaoUsuariosEdit)
    End Sub

    'cria mdi
    Private Sub OpenWindowForUpdate(administracaoUsuariosEdit As administracaousuarios)
        mainModule.OpenWindowForUpdate(Me, administracaoUsuariosEdit)
    End Sub

    Private Sub administracaousuarios_consulta_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.ControlBox = False

        dgDados.DataSource = bindingSource
        ReSearch()

        hideUndesiredColumns(dgDados)
        adjustColumnFormatting(dgDados)

        mainModule.loadRolesCombo(cmbRoles)
        mainModule.loadBloqueadoCombo(cmbBloqueado)
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        ReSearch()
    End Sub

    Private Sub btnNovo_Click(sender As Object, e As EventArgs) Handles btnNovo.Click
        OpenWindowForCreation()
    End Sub

    Private Sub btnExcluirSelecionados_Click(sender As Object, e As EventArgs) Handles btnExcluirSelecionados.Click
        If mainModule.DeleteSelectedItems(dgDados, lblMensagem, "login", "id") Then
            ReSearch()
            treatMessage(lblMensagem, messageDeletedData)
        End If
    End Sub

    Private Sub btnEditarSelecionado_Click(sender As Object, e As EventArgs) Handles btnEditarSelecionado.Click
        If CanEditSelected(dgDados, lblMensagem) Then
            OpenWindowForEdit()
        End If
    End Sub

    Private Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnReport.Click
        mainModule.GenerateReport(Me, mainModule.FORMNAME_ADMUSUARIOS, dgDados, 3)
    End Sub
End Class