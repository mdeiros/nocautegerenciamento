﻿Imports System.Data.OleDb

Public Class TipoBem
    Dim sqlAll As String = "SELECT id, nome FROM tipobem ORDER BY  nome ASC"
    Dim sqlFilterName As String = "SELECT id, nome FROM tipobem WHERE nome LIKE @nome ORDER BY nome ASC"

    Dim messageHelpSearch As String = "O sistema irá buscar qualquer registro " _
        & "que possua um trecho da palavra digitada." _
        & Environment.NewLine & Environment.NewLine _
        & "Para buscar por todos os registros, deixe o campo texto em branco."
    Dim messageHelpGrid As String = "Realize inclusões e alterações diretamente no Grid de dados. " _
        & "As alterações são salvas automaticamente." _
        & Environment.NewLine & Environment.NewLine _
        & "Para excluir itens, selecione a(s) linha(s) desejadas e clique no botão 'Excluir linhas selecionadas'."

    Private Function DefineSQL(name As String)
        If Trim(name) <> "" Then
            Return sqlFilterName
        Else
            Return sqlAll
        End If
    End Function

    Public Sub loadDataGrid(name As String)
        Dim sql = DefineSQL(name)
        Using conn = NGConnection()
            Using command = CType(NGCommand(sql, conn), OleDbCommand)
                If Trim(name) <> "" Then
                    command.Parameters.AddWithValue("@nome", "%" & name & "%")
                End If
                Using adapter = NGAdapter(command)
                    Dim table As DataTable = New DataTable
                    table.Locale = System.Globalization.CultureInfo.InvariantCulture
                    adapter.Fill(table)
                    bindingSource.DataSource = table
                End Using
            End Using
        End Using
    End Sub

    Private Sub resizeGridColumns()
        dgDados.Columns(1).Width = dgDados.Size.Width - 70
    End Sub

    Private Sub saveGrid(name As String, id As Integer)
        save(sqlAll, CType(bindingSource.DataSource, DataTable))

        'para inclusão ou atualização do imobibilizado na criação ou edição do tipo do bem
        If id = 0 Then
            mainModule.InsertUnderImobilizadoOperationType(name)
        Else
            mainModule.UpdateOperationTypeName(name, id)
        End If
    End Sub

    Private Sub ReSearch()
        loadDataGrid(txtNome.Text)
        hideIDColumn(dgDados)
        resizeGridColumns()
        clearMessage(lblMensagem)
    End Sub

    Private Sub TipoBem_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.ControlBox = False
        dgDados.DataSource = bindingSource

        lblHelpSearch.Text = messageHelpSearch
        lblHelpGrid.Text = messageHelpGrid

        ReSearch()
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        ReSearch()
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Dim resp As String
        resp = MsgBox("Deseja excluir os tipos selecionados?", MsgBoxStyle.Question + MsgBoxStyle.YesNo)
        If resp = MsgBoxResult.Yes Then
            If dgDados.SelectedCells.Count = 0 Then
                lblMensagem.Text = messageNoSelectedRecords
            Else
                Try
                    deleteSelectedRowsPerId(dgDados, "tipobem", "id", lblMensagem)

                    ReSearch()
                    treatMessage(lblMensagem, messageDeletedData)
                Catch ex As Exception
                    treatMessage(lblMensagem, ex.Message)
                End Try
            End If
        End If
    End Sub


    Private Sub dgDados_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgDados.CellEndEdit
        Try
            'atualização da grid
            bindingSource.EndEdit()

            Dim name As String = CType(sender, DataGridView).Item(e.ColumnIndex, e.RowIndex).Value

            Dim id As Integer = 0
            'tryparse tenta converter o valor do id antes de atribuir - necessário pq nem todo item tem o id (novo/editado)
            Dim idValue As String = CType(sender, DataGridView).Item(0, e.RowIndex).Value.ToString
            Int32.TryParse(idValue, id)
            saveGrid(name, id)

            ReSearch()
            treatMessage(lblMensagem, messageSavedData)
        Catch ex As Exception
            treatMessage(lblMensagem, ex.Message)
        End Try
    End Sub

    Private Sub btnRelatorio_Click(sender As Object, e As EventArgs) Handles btnRelatorio.Click
        mainModule.GenerateReport(Me, mainModule.FORMNAME_TIPOBEM, dgDados, 1)
    End Sub
End Class