﻿Imports System.Data.OleDb

Public Class lancamento_consulta

    Private Sub loadAndPrepareSearchControls()
        mainModule.formatDateTimePickerInitialPeriodInterval(dtpDataInicio)
        mainModule.formatDateTimePickerFinalPeriodInterval(dtpDataFim)
        loadOperationsCombo()
        LoadOperationTypeCombo()
    End Sub

    Private Sub loadOperationsCombo()
        lancamentoModule.loadOperationsCombo(cmbOperacao)
    End Sub

    Private Sub LoadOperationTypeCombo()
        lancamentoModule.LoadOperationTypeCombo(cmbTipoOperacao)
    End Sub

    Private Function DefineSQL(dataInicio As String, dataFim As String, operacao As String, tipoOperacao As Integer, descricao As String)
        Dim sql = "SELECT l.id, l.operacao, l.idTipoOperacao, l.mesAnoBase, l.descricao, l.valor, " &
            "t.nome as tipoOperacaoNome, t.idPai FROM lancamento l INNER JOIN tipooperacao t ON l.idTipoOperacao=t.id " &
            "WHERE 1=1 "
        If Trim(dataInicio) <> "" And Trim(dataFim) = "" Then
            sql = sql & "AND l.mesAnoBase >= @dataInicio "
        End If
        If Trim(dataInicio) = "" And Trim(dataFim) <> "" Then
            sql = sql & "AND l.mesAnoBase <= @dataFim "
        End If
        If Trim(dataInicio) <> "" And Trim(dataFim) <> "" Then
            sql = sql & "AND l.mesAnoBase BETWEEN @dataInicio AND @dataFim "
        End If
        If Trim(operacao) <> "" Then
            sql = sql & "AND l.operacao=@operacao "
        End If
        If Trim(tipoOperacao) <> 0 Then
            sql = sql & "AND (l.idTipoOperacao=@idTipoOperacao #idTiposOperacao#) "
        End If
        If Trim(descricao) <> "" Then
            sql = sql & "AND l.descricao LIKE @descricao "
        End If
        sql = sql & "ORDER BY l.id ASC"
        Return sql
    End Function

    Private Function DefineCommandWithSQLParameters(sql As String, conn As OleDbConnection, dataInicio As String, dataFim As String, operacao As String, tipoOperacao As Integer, descricao As String)
        Dim command As OleDbCommand = NGCommand(sql, conn)
        If Trim(dataInicio) <> "" And Trim(dataFim) = "" Then
            command.Parameters.AddWithValue("@dataInicio", Convert.ToDateTime(dataInicio))
        End If
        If Trim(dataInicio) = "" And Trim(dataFim) <> "" Then
            command.Parameters.AddWithValue("@dataFim", Convert.ToDateTime(dataFim))
        End If
        If Trim(dataInicio) <> "" And Trim(dataFim) <> "" Then
            command.Parameters.AddWithValue("@dataInicio", Convert.ToDateTime(dataInicio))
            command.Parameters.AddWithValue("@dataFim", Convert.ToDateTime(dataFim))
        End If
        If Trim(operacao) <> "" Then
            command.Parameters.AddWithValue("@operacao", operacao)
        End If
        If Trim(tipoOperacao) <> 0 Then
            command.Parameters.AddWithValue("@idTipoOperacao", tipoOperacao)
        End If
        If Trim(descricao) <> "" Then
            command.Parameters.AddWithValue("@descricao", "%" & descricao & "%")
        End If
        Return command
    End Function

    Private Sub loadTipoOperacaoChildrenIds(idPai As Integer, ids As List(Of Integer))
        Dim sql As String = "SELECT id FROM tipooperacao WHERE idPai=@idPai"
        Dim dt As New DataTable
        Using conn = NGConnection()
            Using cmd = NGCommand(sql, conn)
                'cria datatable com id's
                CType(cmd, OleDbCommand).Parameters.AddWithValue("@idPai", idPai)
                Using da = NGAdapter(cmd)
                    da.Fill(dt)
                End Using
            End Using
        End Using

        For Each row As DataRow In dt.Rows
            Dim id As Integer = Convert.ToInt32(row(0))
            ids.Add(id)
            'fica chamando a fuñção para chamar os filhos dos filhos
            Call loadTipoOperacaoChildrenIds(id, ids)
        Next
    End Sub

    Public Sub loadDataGrid(dataInicio As String, dataFim As String, operacao As String, tipoOperacao As Integer, descricao As String)
        Dim sql = DefineSQL(dataInicio, dataFim, operacao, tipoOperacao, descricao)

        Dim childrenId As New List(Of Integer)
        loadTipoOperacaoChildrenIds(tipoOperacao, childrenId)
        If childrenId.Count > 0 Then
            'ou ele é o id do tipo da operação, ou ele é uma lista de todos os filhos
            sql = sql.replace("#idTiposOperacao#", "Or l.idTipoOperacao IN (" & String.Join(",", childrenId.ToArray) & ") ")
        Else
            sql = sql.replace("#idTiposOperacao#", "")
        End If
        Using conn = NGConnection()
            Using command = DefineCommandWithSQLParameters(sql, conn, dataInicio, dataFim, operacao, tipoOperacao, descricao)
                Using adapter = NGAdapter(command)
                    Dim table As DataTable = New DataTable
                    adapter.Fill(table)
                    bindingSource.DataSource = table
                End Using
            End Using
        End Using
    End Sub

    Private Sub hideUndesiredColumns(grid As DataGridView)
        mainModule.hideUndesiredColumns(grid, {"id", "idTipoOperacao", "idPai"})
    End Sub

    Private Sub adjustColumnFormatting(grid As DataGridView)
        dgDados.Columns(1).HeaderCell.Value = "Operação"
        dgDados.Columns(3).HeaderCell.Value = "Mês/Ano Base"
        dgDados.Columns(3).DefaultCellStyle.Format = mainModule.MonthYearDateFormat()
        dgDados.Columns(4).HeaderCell.Value = "Descrição"
        dgDados.Columns(5).DefaultCellStyle.Format = mainModule.DefaultCurrencyFormat()
        dgDados.Columns(6).HeaderCell.Value = "Tipo de operação"
        dgDados.Columns(6).DisplayIndex = 2
    End Sub

    Public Sub clearSearchFilter()
        cmbOperacao.SelectedIndex = 0
        cmbTipoOperacao.SelectedIndex = 0
        txtDescricao.Text = ""
    End Sub


    Public Sub ReSearch()
        loadDataGrid(dtpDataInicio.Value.Date.ToLongDateString & " 00:00:00",
                     dtpDataFim.Value.Date.ToLongDateString & " 23:59:59",
                     cmbOperacao.SelectedValue, cmbTipoOperacao.SelectedValue, txtDescricao.Text)
        clearMessage(lblMensagem)
        LoadTreeView()
    End Sub

    Private Sub OpenWindowForCreation()
        Dim lancamentoNew As New lancamento()
        lancamentoNew.setData(Me)
        OpenWindowForUpdate(lancamentoNew)
    End Sub

    Private Sub OpenWindowForEdit()
        Dim lancamentoEdit As New lancamento()
        lancamentoEdit.setData(Me, mainModule.OpenWindowForEdit(CType(bindingSource.DataSource, DataTable), dgDados))
        OpenWindowForUpdate(lancamentoEdit)
    End Sub

    Private Sub OpenWindowForUpdate(lancamentoEdit As lancamento)
        mainModule.OpenWindowForUpdate(Me, lancamentoEdit)
    End Sub

    Private Sub LoadTreeView()
        Dim itemName As String = "nome"
        Dim fontName As String = "Consolas"
        Dim fontSize As Integer = 8
        Dim operationTypeTable As DataTable = lancamentoModule.LoadOperationType()

        tvTipoOperacao.Nodes.Clear()
        tvTipoOperacao.BeginUpdate()

        'converte estrutura hierarquica de datatable em tree view
        For i = 0 To operationTypeTable.Rows.Count - 1
            Dim value As Double = 0
            For Each gridRow As DataGridViewRow In dgDados.Rows
                Dim gridOperacao As String = CType(gridRow.Cells(1).Value, String)
                Dim gridTipoOperacaoNome As String = CType(gridRow.Cells(6).Value, String)
                Dim newValue As Double = Convert.ToDouble(gridRow.Cells(5).Value)
                Dim tipoOperacaoItem = operationTypeTable.Rows(i).Item(itemName)
                value = lancamentoModule.calculateTreeViewItemValue(gridOperacao,
                    gridTipoOperacaoNome, newValue, TreeViewItemTextWithoutNumbering(tipoOperacaoItem), value)
            Next
            tvTipoOperacao.Nodes.Add(lancamentoModule.createTreeviewItemText(operationTypeTable, i, itemName, value))
            tvTipoOperacao.Nodes(i).NodeFont = New Font(fontName, fontSize)
            If tvTipoOperacao.Nodes(i).Text.Contains(".-") Then
                tvTipoOperacao.Nodes(i).ForeColor = Color.Red
            End If
        Next

        tvTipoOperacao.EndUpdate()
    End Sub


    Private Sub lancamento_consulta_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.ControlBox = False

        loadAndPrepareSearchControls()

        dgDados.DataSource = bindingSource
        ReSearch()

        hideUndesiredColumns(dgDados)
        adjustColumnFormatting(dgDados)
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        ReSearch()
    End Sub

    Private Sub btnNovo_Click(sender As Object, e As EventArgs) Handles btnNovo.Click
        OpenWindowForCreation()
    End Sub

    Private Sub btnEditarSelecionado_Click(sender As Object, e As EventArgs) Handles btnEditarSelecionado.Click
        If CanEditSelected(dgDados, lblMensagem) Then
            OpenWindowForEdit()
        End If
    End Sub

    Private Sub btnExcluirSelecionados_Click(sender As Object, e As EventArgs) Handles btnExcluirSelecionados.Click
        Dim resp As String
        resp = MsgBox("Deseja excluir os lançamentos selecionados?", MsgBoxStyle.Question + MsgBoxStyle.YesNo)
        If resp = MsgBoxResult.Yes Then
            If mainModule.DeleteSelectedItems(dgDados, lblMensagem, "lancamento", "id") Then
                ReSearch()
                treatMessage(lblMensagem, messageDeletedData)
            End If
        End If
    End Sub

    Private Sub btnRelatorio_Click(sender As Object, e As EventArgs) Handles btnRelatorio.Click
        mainModule.GenerateReport(Me, mainModule.FORMNAME_LANCAMENTO, dgDados, 5)
    End Sub
End Class