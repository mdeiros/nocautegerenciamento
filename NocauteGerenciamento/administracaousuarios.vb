﻿Public Class administracaousuarios
    Dim dtDados As DataTable = New DataTable()
    Dim sql = "SELECT * FROM login"
    Dim editMode As Boolean = False
    Dim baseForm As administracaousuarios_consulta

    'método de passar a tela consulta respectiva
    Public Sub setData(baseForm As Form)
        Me.baseForm = CType(baseForm, administracaousuarios_consulta)
    End Sub

    Public Sub setData(baseForm As Form, dataTable As DataTable)
        Me.baseForm = CType(baseForm, administracaousuarios_consulta)
        editMode = True
        dtDados = dataTable.Copy
        fillFormWithData(dtDados.Rows(0))
    End Sub

    Private Sub fillFormWithData(row As DataRow)
        'carrega os combos
        setInitialValues()
        txtUsername.Text = row("username")
        txtPassword.Text = row("password")
        cmbRole.SelectedValue = row("role")
        cmbBloqueado.SelectedValue = If(row("bloqueado") = True, "1", "0")
        'forma reduzida do if
    End Sub

    'como no método de criação não tem as colunas, como na edição, código para criar colunas na table
    Private Sub prepareDataStructure()
        If Not editMode Then
            AddIntegerColumnIntoTable(dtDados, "id")
            AddStringColumnIntoTable(dtDados, "username")
            AddStringColumnIntoTable(dtDados, "password")
            AddStringColumnIntoTable(dtDados, "role")
            AddBooleanColumnIntoTable(dtDados, "bloqueado")
            mainModule.setAutoIncrementOnFirstColumn(dtDados)
        End If
    End Sub

    Private Sub prepareToSave()
        Dim row As DataRow
        If Not editMode Then
            row = dtDados.NewRow
        Else
            row = dtDados.Rows(0)
        End If

        row("username") = txtUsername.Text
        row("password") = txtPassword.Text
        row("role") = cmbRole.SelectedValue
        row("bloqueado") = If(cmbBloqueado.SelectedValue = "1", True, False)

        If Not editMode Then
            dtDados.Rows.Add(row)
        End If
    End Sub

    Private Sub save()
        mainModule.save(sql, dtDados)
    End Sub

    Private Sub setInitialValues()
        loadRolesCombo()
        loadBloqueadoCombo()
    End Sub

    Private Sub showOpener()
        baseForm.ReSearch()
    End Sub

    Private Sub loadRolesCombo()
        mainModule.loadRolesCombo(cmbRole)
    End Sub

    Private Sub loadBloqueadoCombo()
        mainModule.loadBloqueadoCombo(cmbBloqueado)
    End Sub

    Private Sub administracaousuarios_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.ControlBox = False

        If Not editMode Then
            setInitialValues()
        End If
        prepareDataStructure()
    End Sub

    Private Sub btnGravar_Click(sender As Object, e As EventArgs) Handles btnGravar.Click
        Try
            prepareToSave()
            save()
            Me.Close()
            showOpener()
        Catch ex As Exception
            treatMessage(lblMessage, ex.Message)
        End Try
    End Sub

    Private Sub btnFecharSemSalvar_Click(sender As Object, e As EventArgs) Handles btnFecharSemSalvar.Click
        Me.Close()
        showOpener()
    End Sub

End Class