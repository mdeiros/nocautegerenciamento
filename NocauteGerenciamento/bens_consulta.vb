﻿Imports System.Data.OleDb

Public Class bens_consulta

    Private Sub loadAndPrepareSearchControls()
        mainModule.formatDateTimePickerInitialPeriodInterval(dtpDataInicio)
        mainModule.formatDateTimePickerFinalPeriodInterval(dtpDataFim)
    End Sub

    Private Function DefineSQL(dataInicio As String, dataFim As String, code As String, nfNumber As String, description As String)
        Dim sql = "SELECT b.codPatrimonio, b.descricao, b.numeroNotaFiscal, b.valor, " &
            "b.descricaoDepreciacao, b.prazoMeses, b.dataCriacao, b.taxaMensalDepreciacao, " &
            "b.tipoBem, t.nome as tipoBemNome FROM bem b INNER JOIN tipobem t ON t.id = b.tipoBem " &
            "WHERE 1=1 "
        If Trim(dataInicio) <> "" And Trim(dataFim) = "" Then
            sql = sql & "AND b.dataCriacao >= @dataInicio "
        End If
        If Trim(dataInicio) = "" And Trim(dataFim) <> "" Then
            sql = sql & "AND b.dataCriacao <= @dataFim "
        End If
        If Trim(dataInicio) <> "" And Trim(dataFim) <> "" Then
            sql = sql & "AND b.dataCriacao BETWEEN @dataInicio AND @dataFim "
        End If
        If Trim(code) <> "" Then
            sql = sql & "AND codPatrimonio=@codPatrimonio "
        End If
        If Trim(nfNumber) <> "" Then
            sql = sql & "AND numeroNotaFiscal=@numeroNotaFiscal "
        End If
        If Trim(description) <> "" Then
            sql = sql & "AND descricao like @descricao "
        End If
        sql = sql & "ORDER BY codPatrimonio ASC"
        Return sql
    End Function

    Private Function DefineCommandWithSQLParameters(sql As String, conn As OleDbConnection, dataInicio As String, dataFim As String, code As String, nfNumber As String, description As String)
        Dim command As OleDbCommand = NGCommand(sql, conn)
        If Trim(dataInicio) <> "" And Trim(dataFim) = "" Then
            command.Parameters.AddWithValue("@dataInicio", Convert.ToDateTime(dataInicio))
        End If
        If Trim(dataInicio) = "" And Trim(dataFim) <> "" Then
            command.Parameters.AddWithValue("@dataFim", Convert.ToDateTime(dataFim))
        End If
        If Trim(dataInicio) <> "" And Trim(dataFim) <> "" Then
            command.Parameters.AddWithValue("@dataInicio", Convert.ToDateTime(dataInicio))
            command.Parameters.AddWithValue("@dataFim", Convert.ToDateTime(dataFim))
        End If
        If Trim(code) <> "" Then
            command.Parameters.AddWithValue("@codPatrimonio", code)
        End If
        If Trim(nfNumber) <> "" Then
            command.Parameters.AddWithValue("@numeroNotaFiscal", nfNumber)
        End If
        If Trim(description) <> "" Then
            command.Parameters.AddWithValue("@descricao", "%" & description & "%")
        End If
        Return command
    End Function

    Public Sub loadDataGrid(dataInicio As String, dataFim As String, code As String, nfNumber As String, description As String)
        Dim sql = DefineSQL(dataInicio, dataFim, code, nfNumber, description)
        Using conn = NGConnection()
            Using command = DefineCommandWithSQLParameters(sql, conn, dataInicio, dataFim, code, nfNumber, description)
                Using adapter = NGAdapter(command)
                    Dim table As DataTable = New DataTable
                    adapter.Fill(table)
                    addDynamicColumns(table)
                    bindingSource.DataSource = table
                End Using
            End Using
        End Using
    End Sub

    'criação de novas colunas dinâmicas que não existem no BD
    Public Sub addDynamicColumns(table As DataTable)
        table.Columns.Add("valorADepreciar", Type.GetType("System.Double"))
        table.Columns.Add("valorAtualizado", Type.GetType("System.Double"))
        For Each row As DataRow In table.Rows
            Dim diff As Long = DateDiff(DateInterval.Month, Convert.ToDateTime(row("dataCriacao")), DateTime.Now)
            Dim prazoMeses As Long = Convert.ToInt32(row("prazoMeses"))
            Dim valor As Double = Convert.ToDouble(row("valor"))
            Dim valorADepreciar As Double = 0.0
            If diff <= prazoMeses Then
                valorADepreciar = valor * Convert.ToDouble(row("taxaMensalDepreciacao")) * diff
            Else
                valorADepreciar = valor
            End If
            row("valorADepreciar") = valorADepreciar
            row("valorAtualizado") = valor - valorADepreciar
        Next
    End Sub

    Private Sub hideUndesiredColumns(grid As DataGridView)
        mainModule.hideUndesiredColumns(grid, {"descricaoDepreciacao",
            "taxaMensalDepreciacao", "numeroNotaFiscal", "tipoBem"})
    End Sub

    Private Sub adjustColumnFormatting(grid As DataGridView)
        dgDados.Columns(0).HeaderCell.Value = "Codigo do Patrimonio"
        dgDados.Columns(1).HeaderCell.Value = "Descricao"
        dgDados.Columns(3).HeaderCell.Value = "Valor"
        dgDados.Columns(3).DefaultCellStyle.Format = mainModule.DefaultCurrencyFormat()
        dgDados.Columns(5).HeaderCell.Value = "Prazo (meses)"
        dgDados.Columns(6).HeaderCell.Value = "Data de criação"
        dgDados.Columns(9).HeaderCell.Value = "Tipo do Bem"
        dgDados.Columns(9).DisplayIndex = 1
        dgDados.Columns(10).HeaderCell.Value = "Valor a Depreciar"
        dgDados.Columns(10).DefaultCellStyle.Format = mainModule.DefaultCurrencyFormat()
        dgDados.Columns(11).HeaderCell.Value = "Valor Atualizado"
        dgDados.Columns(11).DefaultCellStyle.Format = mainModule.DefaultCurrencyFormat()
    End Sub

    Public Sub clearSearchFilter()
        txtCodigoPatrimonio.Text = ""
        txtNumeroNotaFiscal.Text = ""
        txtDescricao.Text = ""
    End Sub

    Public Sub ReSearch()
        loadDataGrid(dtpDataInicio.Value.Date.ToLongDateString & " 00:00:00",
                     dtpDataFim.Value.Date.ToLongDateString & " 23:59:59",
                     txtCodigoPatrimonio.Text, txtNumeroNotaFiscal.Text, txtDescricao.Text)
        clearMessage(lblMensagem)
    End Sub

    Private Sub OpenWindowForCreation()
        Dim bensNew As New bens()
        bensNew.setData(Me)
        OpenWindowForUpdate(bensNew)
    End Sub

    Private Sub OpenWindowForEdit()
        Dim bensEdit As New bens()
        bensEdit.setData(Me, mainModule.OpenWindowForEdit(CType(bindingSource.DataSource, DataTable), dgDados))
        OpenWindowForUpdate(bensEdit)
    End Sub

    Private Sub OpenWindowForUpdate(bensEdit As bens)
        mainModule.OpenWindowForUpdate(Me, bensEdit)
    End Sub

    Private Sub bens_consulta_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.ControlBox = False

        loadAndPrepareSearchControls()

        dgDados.DataSource = bindingSource
        ReSearch()

        hideUndesiredColumns(dgDados)
        adjustColumnFormatting(dgDados)
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        ReSearch()
    End Sub

    Private Sub btnNovo_Click(sender As Object, e As EventArgs) Handles btnNovo.Click
        OpenWindowForCreation()
    End Sub

    Private Sub btnExcluirSelecionados_Click(sender As Object, e As EventArgs) Handles btnExcluirSelecionados.Click
        Dim resp As String
        resp = MsgBox("Deseja BAIXAR os bens selecionados?", MsgBoxStyle.Question + MsgBoxStyle.YesNo)
        If resp = MsgBoxResult.Yes Then
            If mainModule.DeleteSelectedItems(dgDados, lblMensagem, "bem", "codPatrimonio") Then
                ReSearch()
                treatMessage(lblMensagem, messageDeletedData)
            End If
        End If
    End Sub

    Private Sub btnEditarSelecionado_Click(sender As Object, e As EventArgs) Handles btnEditarSelecionado.Click
        If CanEditSelected(dgDados, lblMensagem) Then
            OpenWindowForEdit()
        End If
    End Sub

    Private Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnReport.Click
        mainModule.GenerateReport(Me, mainModule.FORMNAME_BEM, dgDados, 8)
    End Sub

End Class