﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class bens
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: o procedimento a seguir é exigido pelo Windows Form Designer
    'Pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tabBem = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmbTipoBem = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtValor = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtNumeroNotaFiscal = New System.Windows.Forms.TextBox()
        Me.lblDataCriacao = New System.Windows.Forms.Label()
        Me.txtDescricao = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tabDepreciacao = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblValorAtualizadoBem = New System.Windows.Forms.Label()
        Me.lblValorDepreciar = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtTaxaMensalDepreciacao = New System.Windows.Forms.TextBox()
        Me.txtPrazoMeses = New System.Windows.Forms.TextBox()
        Me.txtDescricaoDepreciacao = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnSalvar = New System.Windows.Forms.Button()
        Me.btnFecharSemSalvar = New System.Windows.Forms.Button()
        Me.lblMessage = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TabControl1.SuspendLayout()
        Me.tabBem.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.tabDepreciacao.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tabBem)
        Me.TabControl1.Controls.Add(Me.tabDepreciacao)
        Me.TabControl1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(121, 21)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(711, 373)
        Me.TabControl1.TabIndex = 0
        '
        'tabBem
        '
        Me.tabBem.Controls.Add(Me.GroupBox2)
        Me.tabBem.Location = New System.Drawing.Point(4, 25)
        Me.tabBem.Name = "tabBem"
        Me.tabBem.Padding = New System.Windows.Forms.Padding(3)
        Me.tabBem.Size = New System.Drawing.Size(703, 344)
        Me.tabBem.TabIndex = 0
        Me.tabBem.Text = "Bem"
        Me.tabBem.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.cmbTipoBem)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.txtValor)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.txtNumeroNotaFiscal)
        Me.GroupBox2.Controls.Add(Me.lblDataCriacao)
        Me.GroupBox2.Controls.Add(Me.txtDescricao)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(716, 357)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Cadastro/Edição de bem"
        '
        'cmbTipoBem
        '
        Me.cmbTipoBem.FormattingEnabled = True
        Me.cmbTipoBem.Location = New System.Drawing.Point(447, 158)
        Me.cmbTipoBem.Name = "cmbTipoBem"
        Me.cmbTipoBem.Size = New System.Drawing.Size(121, 24)
        Me.cmbTipoBem.TabIndex = 10
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(444, 128)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(89, 16)
        Me.Label9.TabIndex = 9
        Me.Label9.Text = "Tipo do bem"
        '
        'txtValor
        '
        Me.txtValor.Location = New System.Drawing.Point(248, 159)
        Me.txtValor.Name = "txtValor"
        Me.txtValor.Size = New System.Drawing.Size(100, 23)
        Me.txtValor.TabIndex = 7
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(226, 162)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(29, 16)
        Me.Label8.TabIndex = 8
        Me.Label8.Text = "R$ "
        '
        'txtNumeroNotaFiscal
        '
        Me.txtNumeroNotaFiscal.Location = New System.Drawing.Point(248, 86)
        Me.txtNumeroNotaFiscal.Name = "txtNumeroNotaFiscal"
        Me.txtNumeroNotaFiscal.Size = New System.Drawing.Size(100, 23)
        Me.txtNumeroNotaFiscal.TabIndex = 6
        '
        'lblDataCriacao
        '
        Me.lblDataCriacao.AutoSize = True
        Me.lblDataCriacao.Location = New System.Drawing.Point(444, 86)
        Me.lblDataCriacao.Name = "lblDataCriacao"
        Me.lblDataCriacao.Size = New System.Drawing.Size(103, 16)
        Me.lblDataCriacao.TabIndex = 5
        Me.lblDataCriacao.Text = "[data criacao]"
        '
        'txtDescricao
        '
        Me.txtDescricao.Location = New System.Drawing.Point(10, 61)
        Me.txtDescricao.Multiline = True
        Me.txtDescricao.Name = "txtDescricao"
        Me.txtDescricao.Size = New System.Drawing.Size(194, 122)
        Me.txtDescricao.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(444, 60)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(113, 16)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Data de criação"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(245, 128)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Valor"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(245, 60)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(156, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Número da Nota Fiscal"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Descrição"
        '
        'tabDepreciacao
        '
        Me.tabDepreciacao.Controls.Add(Me.GroupBox1)
        Me.tabDepreciacao.Location = New System.Drawing.Point(4, 25)
        Me.tabDepreciacao.Name = "tabDepreciacao"
        Me.tabDepreciacao.Padding = New System.Windows.Forms.Padding(3)
        Me.tabDepreciacao.Size = New System.Drawing.Size(703, 344)
        Me.tabDepreciacao.TabIndex = 1
        Me.tabDepreciacao.Text = "Depreciação"
        Me.tabDepreciacao.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblValorAtualizadoBem)
        Me.GroupBox1.Controls.Add(Me.lblValorDepreciar)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.txtTaxaMensalDepreciacao)
        Me.GroupBox1.Controls.Add(Me.txtPrazoMeses)
        Me.GroupBox1.Controls.Add(Me.txtDescricaoDepreciacao)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(691, 314)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Cadastro/Edição de depreciação do bem"
        '
        'lblValorAtualizadoBem
        '
        Me.lblValorAtualizadoBem.AutoSize = True
        Me.lblValorAtualizadoBem.Location = New System.Drawing.Point(419, 134)
        Me.lblValorAtualizadoBem.Name = "lblValorAtualizadoBem"
        Me.lblValorAtualizadoBem.Size = New System.Drawing.Size(177, 16)
        Me.lblValorAtualizadoBem.TabIndex = 9
        Me.lblValorAtualizadoBem.Text = "[valor atualizado do bem]"
        '
        'lblValorDepreciar
        '
        Me.lblValorDepreciar.AutoSize = True
        Me.lblValorDepreciar.Location = New System.Drawing.Point(244, 134)
        Me.lblValorDepreciar.Name = "lblValorDepreciar"
        Me.lblValorDepreciar.Size = New System.Drawing.Size(131, 16)
        Me.lblValorDepreciar.TabIndex = 8
        Me.lblValorDepreciar.Text = "[valor a depreciar]"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(419, 109)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(165, 16)
        Me.Label11.TabIndex = 7
        Me.Label11.Text = "Valor atualizado do bem"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(244, 109)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(119, 16)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "Valor a depreciar"
        '
        'txtTaxaMensalDepreciacao
        '
        Me.txtTaxaMensalDepreciacao.Location = New System.Drawing.Point(422, 55)
        Me.txtTaxaMensalDepreciacao.Name = "txtTaxaMensalDepreciacao"
        Me.txtTaxaMensalDepreciacao.Size = New System.Drawing.Size(100, 23)
        Me.txtTaxaMensalDepreciacao.TabIndex = 5
        '
        'txtPrazoMeses
        '
        Me.txtPrazoMeses.Location = New System.Drawing.Point(247, 55)
        Me.txtPrazoMeses.Name = "txtPrazoMeses"
        Me.txtPrazoMeses.Size = New System.Drawing.Size(114, 23)
        Me.txtPrazoMeses.TabIndex = 4
        '
        'txtDescricaoDepreciacao
        '
        Me.txtDescricaoDepreciacao.Location = New System.Drawing.Point(10, 55)
        Me.txtDescricaoDepreciacao.Multiline = True
        Me.txtDescricaoDepreciacao.Name = "txtDescricaoDepreciacao"
        Me.txtDescricaoDepreciacao.Size = New System.Drawing.Size(169, 95)
        Me.txtDescricaoDepreciacao.TabIndex = 3
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(419, 27)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(224, 16)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "Taxa mensal de depreciação (%)"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(244, 27)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(102, 16)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Prazo (meses)"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(7, 27)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(178, 16)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Descrição da depreciação"
        '
        'btnSalvar
        '
        Me.btnSalvar.Location = New System.Drawing.Point(121, 412)
        Me.btnSalvar.Name = "btnSalvar"
        Me.btnSalvar.Size = New System.Drawing.Size(75, 23)
        Me.btnSalvar.TabIndex = 1
        Me.btnSalvar.Text = "Salvar"
        Me.btnSalvar.UseVisualStyleBackColor = True
        '
        'btnFecharSemSalvar
        '
        Me.btnFecharSemSalvar.Location = New System.Drawing.Point(202, 412)
        Me.btnFecharSemSalvar.Name = "btnFecharSemSalvar"
        Me.btnFecharSemSalvar.Size = New System.Drawing.Size(114, 23)
        Me.btnFecharSemSalvar.TabIndex = 2
        Me.btnFecharSemSalvar.Text = "Fechar sem salvar"
        Me.btnFecharSemSalvar.UseVisualStyleBackColor = True
        '
        'lblMessage
        '
        Me.lblMessage.Location = New System.Drawing.Point(336, 411)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(392, 23)
        Me.lblMessage.TabIndex = 3
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(0, 293)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(642, 16)
        Me.Label12.TabIndex = 11
        Me.Label12.Text = "*Após o cadastro do bem, necessário o lançamento de seu valor no formulário de la" &
    "nçamentos."
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(1, 309)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(537, 16)
        Me.Label13.TabIndex = 12
        Me.Label13.Text = "*O cadastro de depreciação na aba ""depreciação"" (acima) é campo obrigatório."
        '
        'bens
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(928, 438)
        Me.Controls.Add(Me.lblMessage)
        Me.Controls.Add(Me.btnFecharSemSalvar)
        Me.Controls.Add(Me.btnSalvar)
        Me.Controls.Add(Me.TabControl1)
        Me.KeyPreview = True
        Me.Name = "bens"
        Me.Text = "bens"
        Me.TabControl1.ResumeLayout(False)
        Me.tabBem.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.tabDepreciacao.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents tabBem As TabPage
    Friend WithEvents tabDepreciacao As TabPage
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtValor As TextBox
    Friend WithEvents txtNumeroNotaFiscal As TextBox
    Friend WithEvents lblDataCriacao As Label
    Friend WithEvents txtDescricao As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtTaxaMensalDepreciacao As TextBox
    Friend WithEvents txtPrazoMeses As TextBox
    Friend WithEvents txtDescricaoDepreciacao As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents btnSalvar As Button
    Friend WithEvents Label8 As Label
    Friend WithEvents btnFecharSemSalvar As Button
    Friend WithEvents cmbTipoBem As ComboBox
    Friend WithEvents Label9 As Label
    Friend WithEvents lblValorAtualizadoBem As Label
    Friend WithEvents lblValorDepreciar As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents lblMessage As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label12 As Label
End Class
