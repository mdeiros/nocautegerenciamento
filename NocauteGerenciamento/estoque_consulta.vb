﻿Imports System.Data.OleDb

Public Class estoque_consulta

    Private Sub loadAndPrepareSearchControls()
        mainModule.formatDateTimePickerInitialPeriodInterval(dtpDataInicio)
        mainModule.formatDateTimePickerFinalPeriodInterval(dtpDataFim)
    End Sub

    Private Function DefineSQL(nomeItem As String, dataInicio As String, dataFim As String)
        Dim sql = "SELECT id, nomeItem, qtde, valor, data FROM estoque WHERE 1=1 "
        If Trim(nomeItem) <> "" Then
            sql = sql & "AND nomeItem like @nomeItem "
        End If
        If Trim(dataInicio) <> "" And Trim(dataFim) = "" Then
            sql = sql & "AND data >= @dataInicio "
        End If
        If Trim(dataInicio) = "" And Trim(dataFim) <> "" Then
            sql = sql & "AND data <= @dataFim "
        End If
        If Trim(dataInicio) <> "" And Trim(dataFim) <> "" Then
            sql = sql & "AND data BETWEEN @dataInicio AND @dataFim "
        End If
        sql = sql & "ORDER BY id ASC"
        Return sql
    End Function

    Private Function DefineCommandWithSQLParameters(sql As String, conn As OleDbConnection, nomeItem As String, dataInicio As String, dataFim As String)
        Dim command As OleDbCommand = NGCommand(sql, conn)
        If Trim(nomeItem) <> "" Then
            command.Parameters.AddWithValue("@nomeItem", "%" & nomeItem & "%")
        End If
        If Trim(dataInicio) <> "" And Trim(dataFim) = "" Then
            command.Parameters.AddWithValue("@dataInicio", Convert.ToDateTime(dataInicio))
        End If
        If Trim(dataInicio) = "" And Trim(dataFim) <> "" Then
            command.Parameters.AddWithValue("@dataFim", Convert.ToDateTime(dataFim))
        End If
        If Trim(dataInicio) <> "" And Trim(dataFim) <> "" Then
            command.Parameters.AddWithValue("@dataInicio", Convert.ToDateTime(dataInicio))
            command.Parameters.AddWithValue("@dataFim", Convert.ToDateTime(dataFim))
        End If
        Return command
    End Function

    Public Sub loadDataGrid(nomeItem As String, dataInicio As String, dataFim As String)
        Dim sql = DefineSQL(nomeItem, dataInicio, dataFim)
        Using conn = NGConnection()
            Using command = DefineCommandWithSQLParameters(sql, conn, nomeItem, dataInicio, dataFim)
                Using adapter = NGAdapter(command)
                    Dim table As DataTable = New DataTable
                    adapter.Fill(table)
                    bindingSource.DataSource = table
                End Using
            End Using
        End Using
    End Sub

    Private Sub hideUndesiredColumns(grid As DataGridView)
        mainModule.hideUndesiredColumns(grid, {"id"})
    End Sub

    Private Sub adjustColumnFormatting(grid As DataGridView)
        dgDados.Columns(1).HeaderCell.Value = "Produto"
        dgDados.Columns(1).Width = 210
        dgDados.Columns(2).HeaderCell.Value = "Quantidade"
        dgDados.Columns(2).Width = 150
        dgDados.Columns(3).HeaderCell.Value = "Valor"
        dgDados.Columns(3).Width = 150
        dgDados.Columns(3).DefaultCellStyle.Format = mainModule.DefaultCurrencyFormat()
        dgDados.Columns(4).HeaderCell.Value = "Data"
        dgDados.Columns(4).Width = 150
        dgDados.Columns(4).DefaultCellStyle.Format = mainModule.DefaultDateFormat()
    End Sub

    Public Sub clearSearchFilter()
        txtNomeItem.Text = ""
        dtpDataInicio.Value = ""
    End Sub

    Public Sub ReSearch()
        loadDataGrid(txtNomeItem.Text,
                     dtpDataInicio.Value.Date.ToLongDateString & " 00:00:00",
                     dtpDataFim.Value.Date.ToLongDateString & " 23:59:59")
        clearMessage(lblMensagem)
    End Sub

    Private Sub OpenWindowForCreation()
        Dim estoqueNew As New estoque()
        estoqueNew.setData(Me)
        OpenWindowForUpdate(estoqueNew)
    End Sub

    Private Sub OpenWindowForEdit()
        Dim estoqueEdit As New estoque()
        estoqueEdit.setData(Me, mainModule.OpenWindowForEdit(CType(bindingSource.DataSource, DataTable), dgDados))
        OpenWindowForUpdate(estoqueEdit)
    End Sub

    Private Sub OpenWindowForUpdate(estoqueEdit As estoque)
        mainModule.OpenWindowForUpdate(Me, estoqueEdit)
    End Sub

    Private Sub estoque_consulta_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.ControlBox = False

        loadAndPrepareSearchControls()

        dgDados.DataSource = bindingSource
        ReSearch()

        hideUndesiredColumns(dgDados)
        adjustColumnFormatting(dgDados)
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        ReSearch()
    End Sub

    Private Sub btnNovo_Click(sender As Object, e As EventArgs) Handles btnNovo.Click
        OpenWindowForCreation()
    End Sub

    Private Sub btnExcluirSelecionados_Click(sender As Object, e As EventArgs) Handles btnExcluirSelecionados.Click
        Dim resp As String
        resp = MsgBox("Deseja excluir os selecionados?", MsgBoxStyle.Question + MsgBoxStyle.YesNo)
        If resp = MsgBoxResult.Yes Then
            If mainModule.DeleteSelectedItems(dgDados, lblMensagem, "estoque", "id") Then
                ReSearch()
                treatMessage(lblMensagem, messageDeletedData)
            End If
        End If
    End Sub

    Private Sub btnEditarSelecionado_Click(sender As Object, e As EventArgs) Handles btnEditarSelecionado.Click
        If CanEditSelected(dgDados, lblMensagem) Then
            OpenWindowForEdit()
        End If
    End Sub

    Private Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnReport.Click
        mainModule.GenerateReport(Me, mainModule.FORMNAME_ESTOQUE, dgDados, 4)
    End Sub


End Class