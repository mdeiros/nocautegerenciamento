﻿Imports System.Data.OleDb

Public Class movimento_consulta

    Private Sub loadAndPrepareSearchControls()
        mainModule.formatDateTimePickerInitialPeriodInterval(dtpDataInicio)
        mainModule.formatDateTimePickerFinalPeriodInterval(dtpDataFim)
        loadProdutoCombo()
        LoadTipoMovimentoCombo()
    End Sub

    Private Sub loadProdutoCombo()
        mainModule.loadProdutoCombo(cmbProduto)
    End Sub

    Private Sub LoadTipoMovimentoCombo()
        mainModule.LoadTipoMovimentoCombo(cmbTipoMovimento)
    End Sub

    Private Function DefineSQL(dataInicio As String, dataFim As String, produtoId As String, tipoMovimento As String)
        Dim sql = "SELECT movimento.id, movimento.data, movimento.idItemEstoque, " &
            "estoque.nomeItem, movimento.qtde, movimento.tipoMovimento, estoque.valor " &
            "FROM movimento INNER JOIN estoque ON movimento.idItemEstoque=estoque.id " &
            "WHERE 1=1 "
        'join faz relação entre duas tabelas
        If Trim(dataInicio) <> "" And Trim(dataFim) = "" Then
            sql = sql & "AND movimento.data >= @dataInicio "
        End If
        If Trim(dataInicio) = "" And Trim(dataFim) <> "" Then
            sql = sql & "AND movimento.data <= @dataFim "
        End If
        If Trim(dataInicio) <> "" And Trim(dataFim) <> "" Then
            sql = sql & "AND movimento.data BETWEEN @dataInicio AND @dataFim "
        End If
        If Trim(produtoId) <> "0" Then
            sql = sql & "AND movimento.idItemEstoque=@produtoId "
        End If
        If Trim(tipoMovimento) <> "" Then
            sql = sql & "AND movimento.tipoMovimento=@tipoMovimento "
        End If
        sql = sql & "ORDER BY movimento.id ASC"
        Return sql
    End Function

    Private Function DefineCommandWithSQLParameters(sql As String, conn As OleDbConnection, dataInicio As String, dataFim As String, produtoId As String, tipoMovimento As String)
        Dim command As OleDbCommand = NGCommand(sql, conn)
        If Trim(dataInicio) <> "" And Trim(dataFim) = "" Then
            command.Parameters.AddWithValue("@dataInicio", Convert.ToDateTime(dataInicio))
        End If
        If Trim(dataInicio) = "" And Trim(dataFim) <> "" Then
            command.Parameters.AddWithValue("@dataFim", Convert.ToDateTime(dataFim))
        End If
        If Trim(dataInicio) <> "" And Trim(dataFim) <> "" Then
            command.Parameters.AddWithValue("@dataInicio", Convert.ToDateTime(dataInicio))
            command.Parameters.AddWithValue("@dataFim", Convert.ToDateTime(dataFim))
        End If
        If Trim(produtoId) <> "0" Then
            command.Parameters.AddWithValue("@produtoId", produtoId)
        End If
        If Trim(tipoMovimento) <> "" Then
            command.Parameters.AddWithValue("@tipoMovimento", tipoMovimento)
        End If
        Return command
    End Function

    'a tuple se refere a um par de valores relacionado ao estoque
    Private Sub updateStockWithDeletedMovementQuantities(movements As List(Of Tuple(Of Integer, Integer)))
        Using conn = NGConnection()
            conn.Open()
            For Each movement In movements
                Using cmd = NGCommand("UPDATE estoque SET qtde = qtde + @qtde WHERE id = @id", conn)
                    CType(cmd, OleDbCommand).Parameters.AddWithValue("@qtde", movement.Item2)
                    CType(cmd, OleDbCommand).Parameters.AddWithValue("@id", movement.Item1)
                    'executa uma query que não retorna valores
                    cmd.ExecuteNonQuery()
                End Using
            Next
        End Using
    End Sub

    Public Sub loadDataGrid(dataInicio As String, dataFim As String, produtoId As String, tipoMovimento As String)
        Dim sql = DefineSQL(dataInicio, dataFim, produtoId, tipoMovimento)
        Using conn = NGConnection()
            Using command = DefineCommandWithSQLParameters(sql, conn, dataInicio, dataFim, produtoId, tipoMovimento)
                Using adapter = NGAdapter(command)
                    Dim table As DataTable = New DataTable
                    adapter.Fill(table)
                    bindingSource.DataSource = table
                End Using
            End Using
        End Using
    End Sub

    Private Sub hideUndesiredColumns(grid As DataGridView)
        mainModule.hideUndesiredColumns(grid, {"id", "idItemEstoque"})
    End Sub

    Private Sub adjustColumnFormatting(grid As DataGridView)
        dgDados.Columns(1).HeaderCell.Value = "Data"
        dgDados.Columns(1).Width = 120
        dgDados.Columns(1).DefaultCellStyle.Format = mainModule.DefaultDateFormat()
        dgDados.Columns(3).HeaderCell.Value = "Produto"
        dgDados.Columns(3).Width = 200
        dgDados.Columns(4).HeaderCell.Value = "Quantidade"
        dgDados.Columns(4).Width = 120
        dgDados.Columns(5).HeaderCell.Value = "Tipo de Movimento"
        dgDados.Columns(5).Width = 120
        dgDados.Columns(6).HeaderCell.Value = "Valor"
        dgDados.Columns(6).Width = 120
        dgDados.Columns(6).DefaultCellStyle.Format = mainModule.DefaultCurrencyFormat()
    End Sub

    Public Sub clearSearchFilter()
        dtpDataInicio.Value = ""
        dtpDataFim.Value = ""
        cmbProduto.SelectedIndex = 0
        cmbTipoMovimento.SelectedIndex = 0
    End Sub

    Public Sub ReSearch()
        loadDataGrid(dtpDataInicio.Value.Date.ToLongDateString & " 00:00:00",
                     dtpDataFim.Value.Date.ToLongDateString & " 23:59:59",
                     cmbProduto.SelectedValue, cmbTipoMovimento.SelectedValue)
        clearMessage(lblMensagem)
    End Sub

    Private Sub OpenWindowForCreation()
        Dim movimentoNew As New movimento()
        movimentoNew.setData(Me)
        OpenWindowForUpdate(movimentoNew)
    End Sub

    Private Sub OpenWindowForEdit()
        Dim movimentoEdit As New movimento()
        movimentoEdit.setData(Me, mainModule.OpenWindowForEdit(CType(bindingSource.DataSource, DataTable), dgDados))
        OpenWindowForUpdate(movimentoEdit)
    End Sub

    Private Sub OpenWindowForUpdate(movimentoEdit As movimento)
        mainModule.OpenWindowForUpdate(Me, movimentoEdit)
    End Sub

    Private Sub movimento_consulta_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.ControlBox = False

        loadAndPrepareSearchControls()

        dgDados.DataSource = bindingSource
        ReSearch()

        hideUndesiredColumns(dgDados)
        adjustColumnFormatting(dgDados)
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        ReSearch()
    End Sub

    Private Sub btnNovo_Click(sender As Object, e As EventArgs) Handles btnNovo.Click
        OpenWindowForCreation()
    End Sub

    Private Sub btnExcluirSelecionados_Click(sender As Object, e As EventArgs) Handles btnExcluirSelecionados.Click
        Dim resp As String
        resp = MsgBox("Deseja excluir os lançamentos selecionados?", MsgBoxStyle.Question + MsgBoxStyle.YesNo)
        If resp = MsgBoxResult.Yes Then

            'aqui as tuplas são criadas para atualização de estoque a cada exclusão
            Dim movementsToUpdate As New List(Of Tuple(Of Integer, Integer))
            For Each cell As DataGridViewCell In dgDados.SelectedCells
                Dim row As DataGridViewRow = dgDados.Rows(cell.RowIndex)
                Dim idItemEstoque As Integer = Convert.ToInt32(row.Cells(2).Value)
                Dim qtde As Integer = Convert.ToInt32(row.Cells(4).Value)
                movementsToUpdate.Add(Tuple.Create(idItemEstoque, qtde))
            Next

            If mainModule.DeleteSelectedItems(dgDados, lblMensagem, "movimento", "id") Then
                updateStockWithDeletedMovementQuantities(movementsToUpdate)
                ReSearch()
                treatMessage(lblMensagem, messageDeletedData)
            End If
        End If
    End Sub

    Private Sub btnEditarSelecionado_Click(sender As Object, e As EventArgs) Handles btnEditarSelecionado.Click
        If CanEditSelected(dgDados, lblMensagem) Then
            OpenWindowForEdit()
        End If
    End Sub

    Private Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnReport.Click
        mainModule.GenerateReport(Me, mainModule.FORMNAME_MOVIMENTO, dgDados, 5)
    End Sub

End Class