﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class lancamento
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: o procedimento a seguir é exigido pelo Windows Form Designer
    'Pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblMessage = New System.Windows.Forms.Label()
        Me.btnFecharSemSalvar = New System.Windows.Forms.Button()
        Me.btnGravar = New System.Windows.Forms.Button()
        Me.txtValor = New System.Windows.Forms.TextBox()
        Me.txtDescricao = New System.Windows.Forms.TextBox()
        Me.cmbTipoOperacao = New System.Windows.Forms.ComboBox()
        Me.cmbOperacao = New System.Windows.Forms.ComboBox()
        Me.dpMesAnoBase = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.lblMessage)
        Me.GroupBox1.Controls.Add(Me.btnFecharSemSalvar)
        Me.GroupBox1.Controls.Add(Me.btnGravar)
        Me.GroupBox1.Controls.Add(Me.txtValor)
        Me.GroupBox1.Controls.Add(Me.txtDescricao)
        Me.GroupBox1.Controls.Add(Me.cmbTipoOperacao)
        Me.GroupBox1.Controls.Add(Me.cmbOperacao)
        Me.GroupBox1.Controls.Add(Me.dpMesAnoBase)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(180, 45)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(549, 255)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Cadastro/Edição de Lançamento"
        '
        'lblMessage
        '
        Me.lblMessage.Location = New System.Drawing.Point(7, 184)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(407, 23)
        Me.lblMessage.TabIndex = 12
        '
        'btnFecharSemSalvar
        '
        Me.btnFecharSemSalvar.Location = New System.Drawing.Point(90, 222)
        Me.btnFecharSemSalvar.Name = "btnFecharSemSalvar"
        Me.btnFecharSemSalvar.Size = New System.Drawing.Size(111, 23)
        Me.btnFecharSemSalvar.TabIndex = 11
        Me.btnFecharSemSalvar.Text = "Fechar sem Salvar"
        Me.btnFecharSemSalvar.UseVisualStyleBackColor = True
        '
        'btnGravar
        '
        Me.btnGravar.Location = New System.Drawing.Point(9, 222)
        Me.btnGravar.Name = "btnGravar"
        Me.btnGravar.Size = New System.Drawing.Size(75, 23)
        Me.btnGravar.TabIndex = 10
        Me.btnGravar.Text = "Gravar"
        Me.btnGravar.UseVisualStyleBackColor = True
        '
        'txtValor
        '
        Me.txtValor.Location = New System.Drawing.Point(10, 151)
        Me.txtValor.Name = "txtValor"
        Me.txtValor.Size = New System.Drawing.Size(121, 20)
        Me.txtValor.TabIndex = 9
        '
        'txtDescricao
        '
        Me.txtDescricao.Location = New System.Drawing.Point(268, 95)
        Me.txtDescricao.Name = "txtDescricao"
        Me.txtDescricao.Size = New System.Drawing.Size(254, 20)
        Me.txtDescricao.TabIndex = 8
        '
        'cmbTipoOperacao
        '
        Me.cmbTipoOperacao.FormattingEnabled = True
        Me.cmbTipoOperacao.Location = New System.Drawing.Point(268, 46)
        Me.cmbTipoOperacao.Name = "cmbTipoOperacao"
        Me.cmbTipoOperacao.Size = New System.Drawing.Size(254, 21)
        Me.cmbTipoOperacao.TabIndex = 7
        '
        'cmbOperacao
        '
        Me.cmbOperacao.FormattingEnabled = True
        Me.cmbOperacao.Location = New System.Drawing.Point(10, 46)
        Me.cmbOperacao.Name = "cmbOperacao"
        Me.cmbOperacao.Size = New System.Drawing.Size(232, 21)
        Me.cmbOperacao.TabIndex = 6
        '
        'dpMesAnoBase
        '
        Me.dpMesAnoBase.Location = New System.Drawing.Point(10, 96)
        Me.dpMesAnoBase.Name = "dpMesAnoBase"
        Me.dpMesAnoBase.Size = New System.Drawing.Size(232, 20)
        Me.dpMesAnoBase.TabIndex = 5
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(7, 134)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(31, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Valor"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(265, 79)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(55, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Descrição"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(7, 79)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(77, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Mês/Ano base"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(265, 29)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(91, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Tipo de operação"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Operação"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(4, 194)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(543, 14)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "* Para lançar o valor de um bem, efetue o cadastro premilinar do bem na tela ""Ben" &
    "s"""
        '
        'lancamento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(881, 408)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "lancamento"
        Me.Text = "lancamento"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents btnGravar As Button
    Friend WithEvents txtValor As TextBox
    Friend WithEvents txtDescricao As TextBox
    Friend WithEvents cmbTipoOperacao As ComboBox
    Friend WithEvents cmbOperacao As ComboBox
    Friend WithEvents dpMesAnoBase As DateTimePicker
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents btnFecharSemSalvar As Button
    Friend WithEvents lblMessage As Label
    Friend WithEvents Label6 As Label
End Class
