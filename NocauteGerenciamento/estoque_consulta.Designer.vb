﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class estoque_consulta
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: o procedimento a seguir é exigido pelo Windows Form Designer
    'Pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dtpDataFim = New System.Windows.Forms.DateTimePicker()
        Me.dtpDataInicio = New System.Windows.Forms.DateTimePicker()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtNomeItem = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnReport = New System.Windows.Forms.Button()
        Me.btnExcluirSelecionados = New System.Windows.Forms.Button()
        Me.btnEditarSelecionado = New System.Windows.Forms.Button()
        Me.dgDados = New System.Windows.Forms.DataGridView()
        Me.btnNovo = New System.Windows.Forms.Button()
        Me.bindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.gbMensagem = New System.Windows.Forms.GroupBox()
        Me.lblMensagem = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgDados, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbMensagem.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dtpDataFim)
        Me.GroupBox1.Controls.Add(Me.dtpDataInicio)
        Me.GroupBox1.Controls.Add(Me.btnBuscar)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtNomeItem)
        Me.GroupBox1.Location = New System.Drawing.Point(116, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(719, 116)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Consulta"
        '
        'dtpDataFim
        '
        Me.dtpDataFim.Location = New System.Drawing.Point(521, 54)
        Me.dtpDataFim.Name = "dtpDataFim"
        Me.dtpDataFim.Size = New System.Drawing.Size(90, 20)
        Me.dtpDataFim.TabIndex = 10
        '
        'dtpDataInicio
        '
        Me.dtpDataInicio.Location = New System.Drawing.Point(413, 54)
        Me.dtpDataInicio.Name = "dtpDataInicio"
        Me.dtpDataInicio.Size = New System.Drawing.Size(90, 20)
        Me.dtpDataInicio.TabIndex = 9
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(638, 52)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(75, 23)
        Me.btnBuscar.TabIndex = 8
        Me.btnBuscar.Text = "Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(410, 37)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(45, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Período"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(18, 37)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Produto"
        '
        'txtNomeItem
        '
        Me.txtNomeItem.Location = New System.Drawing.Point(21, 55)
        Me.txtNomeItem.Name = "txtNomeItem"
        Me.txtNomeItem.Size = New System.Drawing.Size(365, 20)
        Me.txtNomeItem.TabIndex = 0
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnReport)
        Me.GroupBox2.Controls.Add(Me.btnExcluirSelecionados)
        Me.GroupBox2.Controls.Add(Me.btnEditarSelecionado)
        Me.GroupBox2.Controls.Add(Me.dgDados)
        Me.GroupBox2.Controls.Add(Me.btnNovo)
        Me.GroupBox2.Location = New System.Drawing.Point(116, 134)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(719, 281)
        Me.GroupBox2.TabIndex = 10
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Grid de dados"
        '
        'btnReport
        '
        Me.btnReport.Location = New System.Drawing.Point(543, 240)
        Me.btnReport.Name = "btnReport"
        Me.btnReport.Size = New System.Drawing.Size(170, 23)
        Me.btnReport.TabIndex = 1
        Me.btnReport.Text = "Relatório"
        Me.btnReport.UseVisualStyleBackColor = True
        '
        'btnExcluirSelecionados
        '
        Me.btnExcluirSelecionados.Location = New System.Drawing.Point(357, 240)
        Me.btnExcluirSelecionados.Name = "btnExcluirSelecionados"
        Me.btnExcluirSelecionados.Size = New System.Drawing.Size(180, 23)
        Me.btnExcluirSelecionados.TabIndex = 3
        Me.btnExcluirSelecionados.Text = "Excluir selecionados"
        Me.btnExcluirSelecionados.UseVisualStyleBackColor = True
        '
        'btnEditarSelecionado
        '
        Me.btnEditarSelecionado.Location = New System.Drawing.Point(172, 240)
        Me.btnEditarSelecionado.Name = "btnEditarSelecionado"
        Me.btnEditarSelecionado.Size = New System.Drawing.Size(179, 23)
        Me.btnEditarSelecionado.TabIndex = 4
        Me.btnEditarSelecionado.Text = "Editar selecionado"
        Me.btnEditarSelecionado.UseVisualStyleBackColor = True
        '
        'dgDados
        '
        Me.dgDados.AllowUserToAddRows = False
        Me.dgDados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgDados.Location = New System.Drawing.Point(7, 20)
        Me.dgDados.Name = "dgDados"
        Me.dgDados.ReadOnly = True
        Me.dgDados.Size = New System.Drawing.Size(706, 200)
        Me.dgDados.TabIndex = 0
        '
        'btnNovo
        '
        Me.btnNovo.Location = New System.Drawing.Point(6, 240)
        Me.btnNovo.Name = "btnNovo"
        Me.btnNovo.Size = New System.Drawing.Size(160, 23)
        Me.btnNovo.TabIndex = 2
        Me.btnNovo.Text = "Novo"
        Me.btnNovo.UseVisualStyleBackColor = True
        '
        'gbMensagem
        '
        Me.gbMensagem.Controls.Add(Me.lblMensagem)
        Me.gbMensagem.Location = New System.Drawing.Point(116, 421)
        Me.gbMensagem.Name = "gbMensagem"
        Me.gbMensagem.Size = New System.Drawing.Size(719, 45)
        Me.gbMensagem.TabIndex = 11
        Me.gbMensagem.TabStop = False
        Me.gbMensagem.Text = "Mensagem"
        '
        'lblMensagem
        '
        Me.lblMensagem.Location = New System.Drawing.Point(7, 19)
        Me.lblMensagem.Name = "lblMensagem"
        Me.lblMensagem.Size = New System.Drawing.Size(453, 23)
        Me.lblMensagem.TabIndex = 0
        '
        'estoque_consulta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(931, 494)
        Me.Controls.Add(Me.gbMensagem)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "estoque_consulta"
        Me.Text = "estoque_consulta"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dgDados, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbMensagem.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents btnBuscar As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtNomeItem As TextBox
    Friend WithEvents dtpDataInicio As DateTimePicker
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents btnReport As Button
    Friend WithEvents btnExcluirSelecionados As Button
    Friend WithEvents btnEditarSelecionado As Button
    Friend WithEvents dgDados As DataGridView
    Friend WithEvents btnNovo As Button
    Friend WithEvents bindingSource As BindingSource
    Friend WithEvents gbMensagem As GroupBox
    Friend WithEvents lblMensagem As Label
    Friend WithEvents dtpDataFim As DateTimePicker
End Class
