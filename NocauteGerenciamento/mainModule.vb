﻿Imports System.Data.OleDb
Imports System.Data.Common

Module mainModule

    Public USERROLE_ADMIN As String = "ADMINISTRADOR"
    Public USERROLE_CONTADOR As String = "CONTADOR"
    Public USERROLE_FUNCIONARIO As String = "FUNCIONARIO"
    Public FORMNAME_ADMUSUARIOS As String = "Administração de usuários"
    Public FORMNAME_TIPOBEM As String = "Tipo de Bem"
    Public FORMNAME_BEM As String = "Bem"
    Public FORMNAME_LANCAMENTO As String = "Lançamento"
    Public FORMNAME_SUPORTE As String = "Suporte"
    Public FORMNAME_ESTOQUE As String = "Estoque"
    Public FORMNAME_MOVIMENTO As String = "Movimentos"
    Public FORMNAME_DADOS As String = "Dados empresariais"

    Public TIPOMOVIMENTO_COMPRA As String = "COMPRA"
    Public TIPOMOVIMENTO_VENDA As String = "VENDA"

    Public messageNoSelectedRecords As String = "Não existem registros selecionados."
    Public messageSavedData As String = "Dados salvos com sucesso"
    Public messageDeletedData As String = "Dados excluídos com sucesso"

    ' role state for the logged user
    Public currentUserRole As String

    ' Application entry point
    Public Sub main()
        Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException)
        Application.EnableVisualStyles()
        Application.SetCompatibleTextRenderingDefault(False)
        Application.Run(New Login)
    End Sub

    ' database utils
    Public myConnectionString As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\database2.mdb;Persist Security Info=False;"

    Public Function NGConnection() As DbConnection
        Return New OleDbConnection(myConnectionString)
    End Function

    Public Function NGCommand(sql As String, conn As DbConnection) As DbCommand
        Return New OleDbCommand(sql, conn)
    End Function

    Public Function NGAdapter(command As DbCommand) As DbDataAdapter
        Return New OleDbDataAdapter(command)
    End Function

    Public Function NGCommandBuilder(adapter As DataAdapter)
        Return New OleDbCommandBuilder(adapter)
    End Function

    Public Sub setAutoIncrementOnFirstColumn(dtDados As DataTable)
        dtDados.Columns(0).AutoIncrement = True
    End Sub

    Public Function InsertUnderImobilizadoOperationType(newItem As String)
        Dim sql As String = "insert into tipooperacao (nome, idTipoBem, idPai) " &
            "SELECT tipobem.nome, tipobem.id, tipooperacao.id FROM tipobem, tipooperacao WHERE tipooperacao.nome = 'Imobilizado' AND tipobem.nome = @newItem"
        Using conn = NGConnection()
            conn.Open()
            Using cmd = NGCommand(sql, conn)
                CType(cmd, OleDbCommand).Parameters.AddWithValue("@newItem", newItem)
                Return cmd.ExecuteNonQuery()
            End Using
        End Using
    End Function

    Public Function UpdateOperationTypeName(name As String, idTipoBem As Integer)
        Dim sql As String = "update tipooperacao set nome=@name where idTipoBem=@idTipoBem"
        Using conn = NGConnection()
            conn.Open()
            Using cmd = NGCommand(sql, conn)
                CType(cmd, OleDbCommand).Parameters.AddWithValue("@name", name)
                CType(cmd, OleDbCommand).Parameters.AddWithValue("@idTipoBem", idTipoBem)
                Return cmd.ExecuteNonQuery()
            End Using
        End Using
    End Function

    Public Function getParentIdFromBem(idTipoBem As Integer)
        Dim sql As String = "SELECT id FROM tipoOperacao where idBem IS NULL AND idTipoBem=@idTipoBem"
        Using conn = NGConnection()
            conn.Open()
            Using cmd = NGCommand(sql, conn)
                CType(cmd, OleDbCommand).Parameters.AddWithValue("@idTipoBem", idTipoBem)
                Return Convert.ToInt32(cmd.ExecuteScalar())
            End Using
        End Using
    End Function

    Public Function InsertOperationRelatedToBem(parentId As Integer, descricaoBem As String)
        Dim sql As String = "insert into tipooperacao (nome, idTipoBem, idBem, idPai) " &
            "SELECT bem.descricao, tipobem.id, bem.codPatrimonio, @parentId FROM " &
                "bem inner join  tipobem on bem.tipobem = tipobem.id " &
                "WHERE bem.descricao = @descricaoBem"
        Using conn = NGConnection()
            conn.Open()
            Using cmd = NGCommand(sql, conn)
                CType(cmd, OleDbCommand).Parameters.AddWithValue("@parentId", parentId)
                CType(cmd, OleDbCommand).Parameters.AddWithValue("@descricaoBem", descricaoBem)
                Return cmd.ExecuteNonQuery()
            End Using
        End Using
    End Function

    Public Function UpdateOperationRelatedToBem(name As String, idBem As Integer)
        Dim sql As String = "update tipooperacao set nome=@name where idBem=@idBem"
        Using conn = NGConnection()
            conn.Open()
            Using cmd = NGCommand(sql, conn)
                CType(cmd, OleDbCommand).Parameters.AddWithValue("@name", name)
                CType(cmd, OleDbCommand).Parameters.AddWithValue("@idBem", idBem)
                Return cmd.ExecuteNonQuery()
            End Using
        End Using
    End Function

    'validation utils
    Public Sub SetIntoRowIfInteger(row As DataRow, rowField As String, rowDesc As String, formValue As String, var As Object)
        If Integer.TryParse(formValue, var) Then
            row(rowField) = var
        Else
            Throw New Exception(rowDesc & " deve ser um número inteiro. Preenchimento de dados depreciação obrigatório.")
        End If
    End Sub
    Public Sub SetIntoRowIfDouble(row As DataRow, rowField As String, rowDesc As String, formValue As String, var As Object)
        If Double.TryParse(formValue, var) Then
            row(rowField) = var
        Else
            Throw New Exception(rowDesc & " deve ser um número decimal. Preenchimento de dados depreciação obrigatório")
        End If
    End Sub
    Public Sub SetColumnValueIntoTextboxIfNotNull(row As DataRow, columnName As String, textField As TextBox)
        If Not IsDBNull(row(columnName)) Then
            textField.Text = row(columnName)
        End If
    End Sub

    ' type functions
    Public Sub AddIntegerColumnIntoTable(dtDados As DataTable, columnName As String)
        dtDados.Columns.Add(columnName, Type.GetType("System.Int32"))
    End Sub
    Public Sub AddStringColumnIntoTable(dtDados As DataTable, columnName As String)
        dtDados.Columns.Add(columnName, Type.GetType("System.String"))
    End Sub
    Public Sub AddDoubleColumnIntoTable(dtDados As DataTable, columnName As String)
        dtDados.Columns.Add(columnName, Type.GetType("System.Double"))
    End Sub
    Public Sub AddDateTimeColumnIntoTable(dtDados As DataTable, columnName As String)
        dtDados.Columns.Add(columnName, Type.GetType("System.DateTime"))
    End Sub
    Public Sub AddBooleanColumnIntoTable(dtDados As DataTable, columnName As String)
        dtDados.Columns.Add(columnName, Type.GetType("System.Boolean"))
    End Sub

    'funções de Internacionalizaçao
    Public Function formatDoubleToBrazilianPortugueseCulture(val As Double)
        Return val.ToString(String.Format("n", System.Globalization.CultureInfo.CreateSpecificCulture("pt-BR")))
    End Function

    Public Function DefaultCurrencyFormat()
        Return String.Format("C", System.Globalization.CultureInfo.CreateSpecificCulture("pt-BR"))
    End Function

    Public Function DefaultDateFormat()
        Return String.Format("d", System.Globalization.CultureInfo.CreateSpecificCulture("pt-BR"))
    End Function

    Public Function MonthYearDateFormat()
        Return String.Format("MMM/yyyy", System.Globalization.CultureInfo.CreateSpecificCulture("pt-BR"))
    End Function

    ' interface utils
    Public Sub treatMessage(lblMessage As Label, message As String)
        lblMessage.Text = message
    End Sub

    Public Sub clearMessage(lblMessage As Label)
        lblMessage.Text = ""
    End Sub

    Public Sub deleteSelectedRowsPerId(dg As DataGridView, tableName As String, idField As String, lblMessage As Label)
        Try
            removeSelectedRowsFromDataGridView(tableName, idField, dg)
        Catch ex As InvalidOperationException
            treatMessage(lblMessage, ex.Message)
        End Try
    End Sub

    Public Sub removeSelectedRowsFromDataGridView(tableName As String, idField As String, dg As DataGridView)
        Using conn = NGConnection()
            Using command = NGCommand("DELETE FROM " & tableName & " WHERE " & idField & " = @id", conn)
                CType(command, OleDbCommand).Parameters.Add("@id", OleDbType.Integer)
                conn.Open()
                For Each cell As DataGridViewCell In dg.SelectedCells
                    Dim row As DataGridViewRow = dg.Rows(cell.RowIndex)
                    command.Parameters(0).Value = row.Cells(0).Value
                    command.ExecuteNonQuery()
                    dg.Rows.Remove(row)
                Next
            End Using
        End Using
    End Sub

    Public Sub clearGridTable(dt As DataTable)
        dt.Clear()
    End Sub

    Public Sub hideIDColumn(dg As DataGridView)
        dg.Columns(0).Visible = False
    End Sub

    Public Sub hideUndesiredColumns(grid As DataGridView, columnsToHide() As String)
        For Each columnToHide As String In columnsToHide
            grid.Columns(columnToHide).Visible = False
        Next
    End Sub

    Public Function save(selectQuery As String, dt As DataTable)
        Using conn = NGConnection()
            Using command = NGCommand(selectQuery, conn)
                Using adapter = NGAdapter(command)
                    Dim cmdbuilder = NGCommandBuilder(adapter)
                    'para evitar que access confunda comando próprio com colunas das tabelas
                    cmdbuilder.QuotePrefix = "["
                    cmdbuilder.QuoteSuffix = "]"
                    Return adapter.Update(dt)
                End Using
            End Using
        End Using
    End Function

    Public Function load(sql As String) As DataTable
        Using conn = NGConnection()
            Using command = NGCommand(sql, conn)
                Using adapter = NGAdapter(command)
                    Dim table As DataTable = New DataTable
                    adapter.Fill(table)
                    Return table
                End Using
            End Using
        End Using
    End Function

    Public Function OpenWindowForEdit(originTable As DataTable, dgDados As DataGridView)
        Dim dtToEdit As DataTable = originTable.Clone
        Dim selectedGridRow As DataGridViewRow = dgDados.Rows(dgDados.SelectedCells.Item(0).RowIndex)

        For Each r As DataRow In originTable.Rows
            If r(0) = selectedGridRow.Cells(0).Value Then
                dtToEdit.ImportRow(r)
                Return dtToEdit
            End If
        Next
    End Function

    Public Sub OpenWindowForUpdate(baseForm As Form, editForm As Form)
        Dim mainForm As main = baseForm.MdiParent
        editForm.MdiParent = mainForm
        editForm.Show()
        editForm.BringToFront()
    End Sub

    Public Function DeleteSelectedItems(dgDados As DataGridView, lblMessage As Label, tableName As String, idField As String)
        If dgDados.SelectedCells.Count = 0 Then
            lblMessage.Text = messageNoSelectedRecords
            Return False
        Else
            Try
                deleteSelectedRowsPerId(dgDados, tableName, idField, lblMessage)
                Return True
            Catch ex As Exception
                lblMessage.Text = ex.Message
                Return False
            End Try
        End If
    End Function

    Public Function CanEditSelected(dgDados As DataGridView, lblMessage As Label)
        If dgDados.SelectedCells.Count = 0 Then
            lblMessage.Text = messageNoSelectedRecords
            Return False
        Else
            Return True
        End If
    End Function

    Public Sub loadRolesCombo(cmb As ComboBox)
        Dim roles As DataTable = New DataTable()
        roles.Columns.Add("COD", Type.GetType("System.String"))
        roles.Columns.Add("VAL", Type.GetType("System.String"))
        roles.Rows.Add("", "Todos")
        roles.Rows.Add(USERROLE_ADMIN, "Administrador")
        roles.Rows.Add(USERROLE_CONTADOR, "Contador")
        roles.Rows.Add(USERROLE_FUNCIONARIO, "Funcionário")

        cmb.DataSource = roles
        cmb.ValueMember = "COD"
        cmb.DisplayMember = "VAL"
    End Sub

    Public Sub loadBloqueadoCombo(cmb As ComboBox)
        Dim bloqueado As DataTable = New DataTable()
        bloqueado.Columns.Add("COD", Type.GetType("System.String"))
        bloqueado.Columns.Add("VAL", Type.GetType("System.String"))
        bloqueado.Rows.Add("", "Todos")
        bloqueado.Rows.Add("0", "Não")
        bloqueado.Rows.Add("1", "Sim")

        cmb.DataSource = bloqueado
        cmb.ValueMember = "COD"
        cmb.DisplayMember = "VAL"
    End Sub

    Public Sub loadTipoMovimentoCombo(cmb As ComboBox)
        Dim data As DataTable = New DataTable()
        data.Columns.Add("COD", Type.GetType("System.String"))
        data.Columns.Add("VAL", Type.GetType("System.String"))
        data.Rows.Add("", "Todos")
        data.Rows.Add(TIPOMOVIMENTO_COMPRA, "Compra")
        data.Rows.Add(TIPOMOVIMENTO_VENDA, "Venda")

        cmb.DataSource = data
        cmb.ValueMember = "COD"
        cmb.DisplayMember = "VAL"
    End Sub

    Public Function loadProduto() As DataTable
        Dim sql As String = "SELECT id, nomeItem FROM estoque ORDER BY id ASC"
        Dim data As DataTable = New DataTable

        Using conn = NGConnection()
            Using command = NGCommand(sql, conn)
                Using adapter = NGAdapter(command)
                    adapter.Fill(data)
                End Using
            End Using
        End Using

        Return data
    End Function

    Public Sub loadProdutoCombo(cmb As ComboBox)
        Dim data As DataTable = loadProduto()
        Dim firstRow As DataRow = data.NewRow
        cmb.DataSource = data
        cmb.ValueMember = "id"
        cmb.DisplayMember = "nomeItem"

        firstRow.Item(0) = 0
        firstRow.Item(1) = "Todos"
        data.Rows.InsertAt(firstRow, 0)
        cmb.SelectedIndex = 0
    End Sub

    'códigos utilitários para relatório
    Public Sub GenerateReport(baseForm As Form, formName As String, dataGridView As DataGridView, columnsToShow As Integer)
        Dim relatorio As relatorio = New relatorio()
        relatorio.setData(dataGridView, formName, columnsToShow)
        Dim mainForm As main = baseForm.MdiParent
        mainForm.prepareFormForMDI(relatorio)
        relatorio.Show()
    End Sub

    'códigos genéricos
    Public Function RemoveWhitespace(fullString As String) As String
        Return New String(fullString.Where(Function(x) Not Char.IsWhiteSpace(x)).ToArray())
    End Function

    Public Function GetDateWithLastDayOfMonth(intMonth, intYear) As Date
        'o 0 dá o último dia do mês.
        Return DateSerial(intYear, intMonth + 1, 0)
    End Function

    Public Function formatDateTimePickerInitialPeriodInterval(dtp As DateTimePicker)
        dtp.CustomFormat = "dd/MM/yyyy"
        dtp.Format = DateTimePickerFormat.Custom
        dtp.Value = New Date(Date.Today.Year, Date.Today.Month, 1)
        Return dtp
    End Function

    Public Function formatDateTimePickerFinalPeriodInterval(dtp As DateTimePicker)
        dtp.CustomFormat = "dd/MM/yyyy"
        dtp.Format = DateTimePickerFormat.Custom
        dtp.Value = mainModule.GetDateWithLastDayOfMonth(Date.Today.Month, Date.Today.Year)
        Return dtp
    End Function

End Module
