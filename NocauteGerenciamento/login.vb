﻿Imports System.Data.OleDb

Partial Public Class Login

    Private userDataTable As DataTable
    Private mensagemUsuarioBloqueado As String = "Usuario bloqueado. Contate o adminstrador."
    Private mensagemUsuarioInvalido As String = "Usuário/senha inválidos."

    Public Sub authenticate(login As String, password As String)
        Me.userDataTable = New DataTable()
        Const sql = "SELECT * FROM login WHERE username=@login AND password=@password"
        Using conn = NGConnection()
            Using command = NGCommand(sql, conn)
                CType(command, OleDbCommand).Parameters.AddWithValue("@login", login)
                CType(command, OleDbCommand).Parameters.AddWithValue("@password", password)
                Using adapter = NGAdapter(command)
                    adapter.Fill(userDataTable)
                End Using
            End Using
        End Using
    End Sub

    Private Sub showErrorMessage(message As String)
        pnMensagemErroLogin.Visible = True
        pnMensagemErroLogin.BackColor = Color.FromArgb(255, 128, 128)
        lblMensagem.Text = message
    End Sub

    Private Sub clearFormFields()
        txtLogin.Text = ""
        txtPassword.Text = ""
    End Sub

    Private Sub SetCurrentUserRole(user As DataRow)
        mainModule.currentUserRole = user("role").ToString()
    End Sub

    Private Sub OpenMainForm()
        Me.Hide()
        Dim main As main = New main()
        main.setLoginForm(Me)
        main.IsMdiContainer = True
        main.Show()
    End Sub

    Public Sub clearFields()
        txtLogin.Text = ""
        txtPassword.Text = ""
    End Sub

    Private Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        authenticate(txtLogin.Text, txtPassword.Text)

        If userDataTable.Rows.Count = 0 Then
            showErrorMessage(mensagemUsuarioInvalido)
            clearFormFields()
        Else
            Dim user As DataRow = userDataTable.Rows(0)

            If Convert.ToBoolean(user("bloqueado")) = True Then
                showErrorMessage(mensagemUsuarioBloqueado)
                clearFormFields()
            Else
                SetCurrentUserRole(user)
                OpenMainForm()
            End If
        End If
    End Sub

End Class