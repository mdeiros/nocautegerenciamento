﻿Imports itextsharp.text
Imports itextsharp.text.pdf
Imports System.IO

Public Class relatorio

    Private dataGridView As DataGridView
    Private reportName As String
    Private columnsToShow As Integer

    Public Sub setData(dataGridView As DataGridView, reportName As String, columnsToShow As Integer)
        Me.dataGridView = dataGridView
        Me.reportName = reportName
        Me.Text = "Relatório: " & Me.reportName
        Me.columnsToShow = columnsToShow
    End Sub

    Private Sub gerarPDF()
        Dim pdfTable As New PdfPTable(columnsToShow)
        pdfTable.DefaultCell.Padding = 10
        pdfTable.WidthPercentage = 100
        pdfTable.HorizontalAlignment = Element.ALIGN_LEFT
        pdfTable.DefaultCell.BorderWidth = 1

        Dim fontHeader As Font = FontFactory.GetFont("Arial", 28, FontStyle.Regular, BaseColor.BLACK)
        Dim fontText As Font = FontFactory.GetFont("Arial", 22, BaseColor.BLACK)

        'dada Header - itera nas colunas visíveis do datagridview e monta as colunas do pdf
        For Each column As DataGridViewColumn In dataGridView.Columns
            If column.Visible Then
                Dim cell As New PdfPCell(New Phrase(column.HeaderText, fontHeader))
                cell.BackgroundColor = New itextsharp.text.BaseColor(240, 240, 240)
                cell.Padding = 10
                pdfTable.AddCell(cell)
            End If
        Next
        'Data itself - monta linhas no pdf com os valores
        For Each row As DataGridViewRow In dataGridView.Rows
            For Each cell As DataGridViewCell In row.Cells
                If dataGridView.Columns.Item(cell.ColumnIndex).Visible Then
                    If cell.Value <> Nothing Then
                        pdfTable.AddCell(New Phrase(cell.Value.ToString(), fontText))
                    Else
                        pdfTable.AddCell(New Phrase(""))
                    End If
                End If
            Next
        Next
        'saving pdf
        Dim fileName As String = "Relatorio" & mainModule.RemoveWhitespace(reportName) & ".pdf"
        Using stream As New FileStream(fileName, FileMode.Create)
            Dim pdfDoc As New Document(PageSize.A2, 10.0F, 10.0F, 10.0F, 0.0F)
            PdfWriter.GetInstance(pdfDoc, stream)
            pdfDoc.Open()

            'escreve o título do relatório 
            Dim reportTitle As Paragraph = New Paragraph(reportName)
            reportTitle.Font.Size = 48
            reportTitle.Font.SetStyle("bold")
            pdfDoc.Add(reportTitle)
            pdfDoc.Add(New Paragraph(" "))
            pdfDoc.Add(New Paragraph(" "))

            'pdftable é a variavel da construção da table
            pdfDoc.Add(pdfTable)
            pdfDoc.Close()
            stream.Close()
        End Using

        axPDF.src = Path.Combine(My.Application.Info.DirectoryPath, fileName)
        lblFilePath.Text = axPDF.src
    End Sub

    Private Sub relatorio_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.ControlBox = False
        gerarPDF()
    End Sub

    Private Sub btnFechar_Click(sender As Object, e As EventArgs) Handles btnFechar.Click
        Me.Close()
        main.showForm(Me.reportName)
    End Sub
End Class