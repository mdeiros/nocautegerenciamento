﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class main
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: o procedimento a seguir é exigido pelo Windows Form Designer
    'Pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(main))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.AdministraçãoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MovimentosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EstoqueToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MovimentosToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.GestãoContábilToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TipoDeBemToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.BensToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.LançamentosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AjudaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SuporteToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.btnDados = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.AutoSize = False
        Me.MenuStrip1.BackColor = System.Drawing.Color.SlateGray
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AdministraçãoToolStripMenuItem, Me.MovimentosToolStripMenuItem, Me.GestãoContábilToolStripMenuItem, Me.btnDados, Me.AjudaToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(928, 58)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'AdministraçãoToolStripMenuItem
        '
        Me.AdministraçãoToolStripMenuItem.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.AdministraçãoToolStripMenuItem.Name = "AdministraçãoToolStripMenuItem"
        Me.AdministraçãoToolStripMenuItem.Size = New System.Drawing.Size(112, 54)
        Me.AdministraçãoToolStripMenuItem.Text = "Administração"
        '
        'MovimentosToolStripMenuItem
        '
        Me.MovimentosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EstoqueToolStripMenuItem, Me.MovimentosToolStripMenuItem1})
        Me.MovimentosToolStripMenuItem.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MovimentosToolStripMenuItem.Name = "MovimentosToolStripMenuItem"
        Me.MovimentosToolStripMenuItem.Size = New System.Drawing.Size(97, 54)
        Me.MovimentosToolStripMenuItem.Text = "Operacional"
        '
        'EstoqueToolStripMenuItem
        '
        Me.EstoqueToolStripMenuItem.Name = "EstoqueToolStripMenuItem"
        Me.EstoqueToolStripMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.EstoqueToolStripMenuItem.Text = "Estoque"
        '
        'MovimentosToolStripMenuItem1
        '
        Me.MovimentosToolStripMenuItem1.Name = "MovimentosToolStripMenuItem1"
        Me.MovimentosToolStripMenuItem1.Size = New System.Drawing.Size(154, 22)
        Me.MovimentosToolStripMenuItem1.Text = "Movimentos"
        '
        'GestãoContábilToolStripMenuItem
        '
        Me.GestãoContábilToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TipoDeBemToolStripMenuItem1, Me.BensToolStripMenuItem1, Me.LançamentosToolStripMenuItem})
        Me.GestãoContábilToolStripMenuItem.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GestãoContábilToolStripMenuItem.Name = "GestãoContábilToolStripMenuItem"
        Me.GestãoContábilToolStripMenuItem.Size = New System.Drawing.Size(124, 54)
        Me.GestãoContábilToolStripMenuItem.Text = "Gestão Contábil"
        '
        'TipoDeBemToolStripMenuItem1
        '
        Me.TipoDeBemToolStripMenuItem1.Name = "TipoDeBemToolStripMenuItem1"
        Me.TipoDeBemToolStripMenuItem1.Size = New System.Drawing.Size(163, 22)
        Me.TipoDeBemToolStripMenuItem1.Text = "Tipo de Bem"
        '
        'BensToolStripMenuItem1
        '
        Me.BensToolStripMenuItem1.Name = "BensToolStripMenuItem1"
        Me.BensToolStripMenuItem1.Size = New System.Drawing.Size(163, 22)
        Me.BensToolStripMenuItem1.Text = "Bens"
        '
        'LançamentosToolStripMenuItem
        '
        Me.LançamentosToolStripMenuItem.Name = "LançamentosToolStripMenuItem"
        Me.LançamentosToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.LançamentosToolStripMenuItem.Text = "Lançamentos"
        '
        'AjudaToolStripMenuItem
        '
        Me.AjudaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SuporteToolStripMenuItem1})
        Me.AjudaToolStripMenuItem.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AjudaToolStripMenuItem.Name = "AjudaToolStripMenuItem"
        Me.AjudaToolStripMenuItem.Size = New System.Drawing.Size(57, 54)
        Me.AjudaToolStripMenuItem.Text = "&Ajuda"
        '
        'SuporteToolStripMenuItem1
        '
        Me.SuporteToolStripMenuItem1.Name = "SuporteToolStripMenuItem1"
        Me.SuporteToolStripMenuItem1.Size = New System.Drawing.Size(152, 22)
        Me.SuporteToolStripMenuItem1.Text = "&Suporte"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(45, 54)
        Me.ExitToolStripMenuItem.Text = "Sai&r"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.SlateGray
        Me.Label1.Font = New System.Drawing.Font("Verdana", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(640, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(212, 32)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "NOCAUTE DB"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.SlateGray
        Me.PictureBox1.BackgroundImage = CType(resources.GetObject("PictureBox1.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox1.Location = New System.Drawing.Point(851, 7)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(54, 51)
        Me.PictureBox1.TabIndex = 4
        Me.PictureBox1.TabStop = False
        '
        'btnDados
        '
        Me.btnDados.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDados.Name = "btnDados"
        Me.btnDados.Size = New System.Drawing.Size(144, 54)
        Me.btnDados.Text = "Dados Empresariais"
        '
        'main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.ClientSize = New System.Drawing.Size(928, 539)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "main"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "NocauteGerenciamento"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ExitToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AjudaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SuporteToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents GestãoContábilToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MovimentosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TipoDeBemToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents BensToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents LançamentosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Label1 As Label
    Friend WithEvents AdministraçãoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EstoqueToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MovimentosToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents btnDados As ToolStripMenuItem
End Class
