﻿Imports System.Data.OleDb

Public Class movimento

    Const messageStockAfterSale As String = "A venda deixará o estoque negativo, e isto não é permitido."

    Public Function increaseEstoque(valueToIncrease As Integer, id As Integer)
        Dim sql As String = "UPDATE estoque SET qtde = qtde + @valueToIncrease " &
            "WHERE id=@id"
        Using conn = NGConnection()
            conn.Open()
            Using cmd = NGCommand(sql, conn)
                CType(cmd, OleDbCommand).Parameters.AddWithValue("@valueToIncrease", valueToIncrease)
                CType(cmd, OleDbCommand).Parameters.AddWithValue("@id", id)
                Return cmd.ExecuteNonQuery()
            End Using
        End Using
    End Function

    Public Function decreaseEstoque(valueToDecrease As Integer, id As Integer)
        Dim sql As String = "UPDATE estoque SET qtde = qtde - @valueToDecrease " &
            "WHERE id=@id"
        Using conn = NGConnection()
            conn.Open()
            Using cmd = NGCommand(sql, conn)
                CType(cmd, OleDbCommand).Parameters.AddWithValue("@valueToDecrease", valueToDecrease)
                CType(cmd, OleDbCommand).Parameters.AddWithValue("@id", id)
                Return cmd.ExecuteNonQuery()
            End Using
        End Using
    End Function

    Private Sub loadAndPrepareSearchControls()
        loadProdutoCombo()
        LoadTipoMovimentoCombo()
    End Sub

    Private Sub loadProdutoCombo()
        mainModule.loadProdutoCombo(cmbProduto)
    End Sub

    Private Sub LoadTipoMovimentoCombo()
        mainModule.loadTipoMovimentoCombo(cmbTipoMovimento)
    End Sub

    Dim dtDados As DataTable = New DataTable()
    Dim sql = "SELECT * FROM movimento"
    Dim editMode As Boolean = False
    Dim baseForm As movimento_consulta

    Public Sub setData(baseForm As Form)
        Me.baseForm = CType(baseForm, movimento_consulta)
    End Sub

    Public Sub setData(baseForm As Form, dataTable As DataTable)
        Me.baseForm = CType(baseForm, movimento_consulta)
        editMode = True
        dtDados = dataTable.Copy
        fillFormWithData(dtDados.Rows(0))
    End Sub

    Private Sub fillFormWithData(row As DataRow)
        setInitialValues()
        dtpData.Value = Convert.ToDateTime(row("data"))
        cmbProduto.SelectedValue = row("idItemEstoque")
        txtQuantidade.Text = Convert.ToInt32(row("qtde"))
        cmbTipoMovimento.SelectedValue = row("tipoMovimento")

    End Sub

    Private Sub prepareDataStructure()
        If Not editMode Then
            AddIntegerColumnIntoTable(dtDados, "id")
            AddDateTimeColumnIntoTable(dtDados, "data")
            AddIntegerColumnIntoTable(dtDados, "idItemEstoque")
            AddIntegerColumnIntoTable(dtDados, "qtde")
            AddStringColumnIntoTable(dtDados, "tipoMovimento")
            setAutoIncrementOnFirstColumn(dtDados)
        End If
    End Sub

    Private Function validateSavingPreConditions()
        ' can sale only of stock remains > 0 after sale
        If cmbTipoMovimento.SelectedValue = mainModule.TIPOMOVIMENTO_VENDA Then
            If Not validateStockShouldBeNonNegativeAfterSale(Convert.ToInt32(txtQuantidade.Text),
                Convert.ToInt32(cmbProduto.SelectedValue)) Then
                lblMessage.Text = messageStockAfterSale
                Return False
            End If
        End If
        Return True
    End Function

    Private Function validateStockShouldBeNonNegativeAfterSale(newQtde As Integer, id As Integer)
        Dim sql As String = "select qtde - @newQtde from estoque where id=@id"
        Using conn = NGConnection()
            conn.Open()
            Using cmd = NGCommand(sql, conn)
                CType(cmd, OleDbCommand).Parameters.AddWithValue("@newQtde", newQtde)
                CType(cmd, OleDbCommand).Parameters.AddWithValue("@id", id)
                Return Convert.ToInt32(cmd.ExecuteScalar()) >= 0
            End Using
        End Using

    End Function

    Private Sub prepareToSave()
        Dim idItemEstoque As Integer
        Dim qtde As Integer

        Dim row As DataRow
        If Not editMode Then
            row = dtDados.NewRow
        Else
            row = dtDados.Rows(0)
        End If

        row("data") = dtpData.Value
        SetIntoRowIfInteger(row, "idItemEstoque", "Produto", cmbProduto.SelectedValue, idItemEstoque)
        SetIntoRowIfInteger(row, "qtde", "Qtde", txtQuantidade.Text, qtde)
        row("tipoMovimento") = cmbTipoMovimento.SelectedValue

        If Not editMode Then
            dtDados.Rows.Add(row)
        End If
    End Sub

    Private Sub save()
        mainModule.save(sql, dtDados)
    End Sub

    Private Sub updateEstoque()
        If cmbTipoMovimento.SelectedValue = TIPOMOVIMENTO_COMPRA Then
            increaseEstoque(Convert.ToInt32(txtQuantidade.Text), Convert.ToInt32(cmbProduto.SelectedValue))
        ElseIf cmbTipoMovimento.SelectedValue = TIPOMOVIMENTO_VENDA Then
            decreaseEstoque(Convert.ToInt32(txtQuantidade.Text), Convert.ToInt32(cmbProduto.SelectedValue))
        End If
    End Sub

    Private Sub setInitialValues()
        dtpData.CustomFormat = "dd/MM/yyyy"
        dtpData.Format = DateTimePickerFormat.Custom
        loadAndPrepareSearchControls()
    End Sub

    Private Sub showOpener()
        baseForm.ReSearch()
    End Sub

    Private Sub movimento_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.ControlBox = False

        If Not editMode Then
            setInitialValues()
        End If
        prepareDataStructure()
    End Sub

    Private Sub btnGravar_Click(sender As Object, e As EventArgs) Handles btnGravar.Click
        If cmbProduto.SelectedIndex = 0 Or txtQuantidade.Text = "" Or cmbTipoMovimento.SelectedIndex = 0 Then
            MsgBox("Preencha todos os campos")
        Else
            Try
                If validateSavingPreConditions() Then
                    prepareToSave()
                    save()
                    updateEstoque()
                    Me.Close()
                    showOpener()
                End If
            Catch ex As Exception
                treatMessage(lblMessage, ex.Message)
            End Try
        End If
    End Sub

    Private Sub btnFecharSemSalvar_Click(sender As Object, e As EventArgs) Handles btnFecharSemSalvar.Click
        Me.Close()
        showOpener()
    End Sub

End Class